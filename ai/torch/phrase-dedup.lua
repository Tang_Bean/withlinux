
local function get_word_count(sent_)
	-- @input sent is a string
	-- @output a table containing frequency of words
	local sent = sent_ or ''
	local freq = {}
	local word_list = stringx.split(sent)
	--table.sort(word_list)
	--print(word_list)
	for i = 1, #word_list do
		if not freq[word_list[i]] then
			freq[word_list[i]] = 1
		else
			freq[word_list[i]] = freq[word_list[i]] + 1
		end
	end
	return freq
end

local function warn_word_count(sent_)
	-- @input sent_ is string
	-- @output nil
	local freq = get_word_count(sent_)
	for k, v in pairs(freq) do
		print("W: word ["..k.."] duplicated for "..tostring(v).." times.")
	end
end

print(get_word_count("hi hello hi hi hil "))
print(warn_word_count("hi hello hi hi hi"))

local function proposal_purge_stopwords(prop_)
	-- @input prop_ is a table of proposals
	-- @output a table of processed proposals
	local prop = {}
	local stopwords = {"a", "the"} -- @config
	local function _in_ (word_, tab_)
		for k_, v_ in pairs(tab_) do
			if word_ == v_ then
				return true
			end
		end
		return false
	end
	local function remove_words(sent_, words_)
		-- @pipeline split orig sent into a list, remove specified, join
		local sent = stringx.split(sent_)
		local newsent = {}
		for k_, v_ in pairs(sent) do
			if not _in_(v_, words_) then
				table.insert(newsent, v_)
			end
		end
		return stringx.join(" ", newsent)
	end
	-- begin to work
	for k, v in pairs(prop_) do
		table.insert(prop,
			{remove_words(prop_[k][1], stopwords), prop_[k][2],
			 remove_words(prop_[k][3], stopwords), prop_[k][4]}
		)
	end
	return prop
end
local function proposal_dedup_after_purging_stopwords(prop_)
	-- @input prop_ is a table of proposals
	local function _p_in_plist_(p, plist)
		for k_, v_ in pairs(plist) do
			if (p[1] == v_[1]) and (p[2] == v_[2]) and (p[3] == v_[3]) then
				return true
			end
		end
		return false
	end
	local newprop = {}
	for k, v in pairs(prop_) do
		if not _p_in_plist_(v, newprop) then
			table.insert(newprop, v)
		end
	end
	return newprop
end

local my_prop = {
		{"asdf", 2, "asd", 0},
		{"a asdf", 2, "asd", 0.5},
		{"the asdf", 2, "asd", 0},
		{"the asdf", 3, "asd", 0},
		{"asdf kljwlf", 2, "asasdfd", 0},
	}
print("my prop")
print(my_prop)
local clean_prop = proposal_purge_stopwords(my_prop)
print("clean prop")
print(clean_prop)
local dedup_clean_prop = proposal_dedup_after_purging_stopwords(clean_prop)
print("dedup_clean_prop")
print(dedup_clean_prop)

local function phraselist_purge_stopwords(prop_)
	-- @input prop_ is a table of strings
	-- @output a table of processed strings
	local prop = {}
	local stopwords = {"a", "the"} -- @config
	local function _in_ (word_, tab_)
		for k_, v_ in pairs(tab_) do
			if word_ == v_ then
				return true
			end
		end
		return false
	end
	local function remove_words(sent_, words_)
		-- @pipeline split orig sent into a list, remove specified, join
		local sent = stringx.split(sent_)
		local newsent = {}
		for k_, v_ in pairs(sent) do
			if not _in_(v_, words_) then
				table.insert(newsent, v_)
			end
		end
		return stringx.join(" ", newsent)
	end
	-- begin to work
	for k, v in pairs(prop_) do
		table.insert(prop, remove_words(v, stopwords))
	end
	return prop
end
local function phraselist_dedup_after_purging_stopwords(prop_)
	-- @input prop_ is a table of proposals
	local function _p_in_plist_(p, plist)
		for k_, v_ in pairs(plist) do
			if (p == v_) then
				return true
			end
		end
		return false
	end
	local newprop = {}
	for k, v in pairs(prop_) do
		if not _p_in_plist_(v, newprop) then
			table.insert(newprop, v)
		end
	end
	return newprop
end

local my_phraselist = {
		"asdf",
		"a asdf",
		"the asdf",
		"the asdf",
		"asdf kljwlf",
	}

print(my_phraselist)
local clean_phraselist = phraselist_purge_stopwords(my_phraselist)
print(clean_phraselist)
local dedup_clean_phraselist = phraselist_dedup_after_purging_stopwords(clean_phraselist)
print(dedup_clean_phraselist)
