
require 'nn'

a = torch.rand(10,1)
print(a)
l = nn.TemporalMaxPooling(2)
print(l)
b = l:forward(a)
print(b)
da = l:backward(a, b)
print(da)

local mp = require 'mypooling'

for size = 10,640,10 do
	local a = torch.rand(size)

	local l = nn.MyPooling(size, 2)
	local lp = nn.TemporalMaxPooling(2)

	local b = l:forward(a)
	local bp = lp:forward(a:view(size, 1))
	print(size, (b-bp):norm())

	local da = l:backward(a, b)
	local dap = lp:backward(a:view(size, 1), bp)
	print(size, (da-dap):norm())
end
