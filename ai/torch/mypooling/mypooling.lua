mypooling = {}

local MyPooling, parent = torch.class('nn.MyPooling', 'nn.Module')

function MyPooling:__init(inputSize, kW)
	parent.__init(self)
	self.inputSize = inputSize
	if kW ~= 2 then error('kW must be 2') end
	self.kW = kW
	self.mask = torch.zeros(inputSize)
	self.output = torch.zeros(inputSize/kW)
	self.gradInput = torch.zeros(inputSize)
end

function MyPooling:updateOutput(input)
	if input:dim() ~= 1 then
		error('input dim must be 1')
	else
		self.output:zero()
		self.mask:zero()
		for i = 1, self.inputSize, 2 do
			--self.output[(i+1)/2] = (input[i] > input[i+1]) and input[i] or input[i+1]
			if input[i] > input[i+1] then
				self.output[(i+1)/2] = input[i]
				self.mask[i] = 1.0
			else
				self.output[(i+1)/2] = input[i+1]
				self.mask[i+1] = 1.0
			end
		end
	end
	return self.output
end

function MyPooling:updateGradInput(input, gradOutput)
	self.gradInput:zero()
	self.gradInput:copy( self.mask )
	local cursor = 1
	for i = 1, self.inputSize do
		if self.gradInput[i] > 0.0 then
			self.gradInput[i] = gradOutput[cursor]
			cursor = cursor + 1
		end
	end
	if cursor ~= self.inputSize/2 + 1 then
		error('sum of mask weird')
	end
	return self.gradInput
end

return mypooling
