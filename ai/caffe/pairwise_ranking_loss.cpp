/**
 * Reference Implementation of Pairwise Ranking Loss in Caffe
 * XXX: This piece of code is definitely not working!
 *
 * reference python (theano) code from Kiros (visualize ...)
 */


                                                                 // Header Part
#ifndef CAFFE_PWL_LOSS_LAYER_HPP_
#define CAFFE_PWL_LOSS_LAYER_HPP_

#include <vector>

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

#include "caffe/layers/loss_layer.hpp"

namespace caffe {

template <typename Dtype>
class PairwiseRankingLossLayer : public LossLayer<Dtype> {
	public:
		explicit PairwiseRankingLossLayer(const LayerParameter& param)
			: LossLayer<Dtype>(param), diff_() {}
		virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
				const vector<Blob<Dtype>*>& top);

		virtual inline int ExactNumBottomBlobs() const { return 2; }
		virtual inline const char* type() const { return "PairwiseRankingLoss"; }

		// Backpropagate to both inputs
		virtual inline bool AllowForceBackward(const int bottom_index) const {
			return bottom_index < 2;
		}

	protected:
		// Layer Assumptions:
		// 1. All input vectors are normalized, i.e. $x^T x = 1$ for all x
		// 2. Two input sources,
		//    1) X stands for the CNN feature set
		//       size: batchsize * 1 * 1 * featuredim
		//    2) V stands for the LSTM feature set
		//       size: batchsize * 1 * 1 * featuredim
		virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
				const vector<Blob<Dtype>*>& top);
		//virtual void Forward_gpu // FIXME: CUDA version not implemented
	
		virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
				const vector<bool>& propagate_down,
				const vector<Blob<Dtype>*>& bottom);
		//virtual void Backward_gpu // FIXME: CUDA version not implemented
		
		Blob<Dtype> data_;
		Blob<Dtype> diff_;
		Blob<Dtype> scores_;
		Blob<Dtype> lossmat_lstm_;
		Blob<Dtype> lossmat_cnn_;

		// pebpxi_ means "Partial Loss(x) by partial x_i"
		// pebpxi_ means " ∂ Loss(x) / ∂ x_i"
		Blob<Dtype> pebpxi_;
		// pebpvi_ means "Partial Loss(x) by partial v_i"
		// pebpvi_ means " ∂ Loss(x) / ∂ v_i"
		Blob<Dtype> pebpvi_;

		// configurable parameter. If you'd like a more elegant
		// implementation, please add new protobuf definition for this layer.
		Dtype margin_ = 0.1;
}

} // namespace caffe

#endif // CAFFE_PWL_LOSS_LAYER_HPP_



                                                                    // SRC Part
																	
#include <algorithm>
#include <vector>

#include "caffe/util/math_functions.hpp"

namespace caffe {

	                                                              // LayerSetUp
template <typename Dtype>
void PairwiseRankingLossLayer<Dtype>::LayerSetUp(
		const vector<Blob<Dtype>*>& bottom,
		const vector<Blob<Dtype>*>& top) {

	LossLayer<Dtype>::LayerSetUp(bottom, top);

	// Blob shape(num, channels, height, width) Checking
	CHECK_EQ(bottom[0]->num(), bottom[1]->num()); // batch size
	CHECK_EQ(bottom[0]->channels(), 1);
	CHECK_EQ(bottom[1]->channels(), 1);
	CHECK_EQ(bottom[0]->height(), 1);
	CHECK_EQ(bottom[1]->height(), 1);
	CHECK_EQ(bottom[0]->width(), bottom[1]->width()); // feature vec dim

	// Reshape our protected Blobs internal temporary storage
	scores_.Reshape(1, 1, bottom[0]->num(), bottom[1]->num());
	lossmat_cnn_.Reshape(1, 1, bottom[0]->num(), bottom[1]->num());
	lossmat_lstm_.Reshape(1, 1, bottom[0]->num(), bottom[1]->num());
	pebpxi_.Reshape(1, 1, 1, bottom[0]->width());
	pebpvi_.Reshape(1, 1, 1, bottom[0]->width());
}

                                                                     // Forward
template <typename Dtype>
void PairwiseRankingLossLayer<Dtype>::Forward_cpu(
		const vector<Blob<Dtype>*>& bottom,
		const vector<Blob<Dtype>*>& top) {

	// FIXME: Parallelize with OpenMP, or the performance will be awkward.

	// calc dot prod every vector pairs
	// for x_i, v_j: scores_[i,j] = x_i * v_j
	//   where i in batchsize, j in batchsize
	int batchsize = bottom[0]->num();
	for (int i = 0; i < batchsize; i++) {
		for (int j = 0; j < batchsize; j++) {
			scores_.mutalbe_cpu_data()[scores_.offset(0,0,i,j)] = 
				caffe_cpu_dot(bottom[0]->width(),
					bottom[0]->data_at(i,0,0,0), bottom[1]->data_at(j,0,0,0));
		}
	}

	// calculate loss : lstm loss and cnn loss
	// 1) lossmat_xx_[:,:] = margin_
	caffe_set(lossmat_cnn_.count(), margin_, lossmat_cnn_.mutable_cpu_data());
	caffe_set(lossmat_lstm_.count(), margin_, lossmat_cnn_.mutable_cpu_data());
	// 2) - diagonal(scores_[i,i]) + scores_[i,j]
	for (int i = 0; i < batchsize; i++) {
		for (int j = 0; j < batchsize; j++) {
			lossmat_cnn_.mutable_cpu_data()[lossmat_cnn_.offset(0,0,i,j)]
				+= -scores_.data_at(0,0,i,i) + scores_.data_at(0,0,i,j); //ij
			lossmat_lstm_.mutable_cpu_data()[lossmat_lstm_.offset(0,0,i,j)]
				+= -scores_.data_at(0,0,i,i) + scores_.data_at(0,0,j,i); //ji
		}
	}
	// 3) lossmat = hinge activation (lossmat), fill diagonal with 0.
	for (int i = 0; i < batchsize; i++) {
		for (int j = 0; j < batchsize; j++) {
			if (i == j) {
				lossmat_cnn_.mutable_cpu_data()[lossmat_cnn_.offset(0,0,i,j)]
					= (Dtype)0.;
				lossmat_lstm_.mutable_cpu_data()[lossmat_lstm_.offset(0,0,i,j)]
					= (Dtype)0.;
				continue;
			}
			if (lossmat_cnn_.data_at(0,0,i,j) < (Dtype)0.) {
				lossmat_cnn_.mutable_cpu_data()[lossmat_cnn_.offset(0,0,i,j)]
					= (Dtype)0.;
			}
			if (lossmat_lstm_.data_at(0,0,i,j) < (Dtype)0.) {
				lossmat_lstm_.mutable_cpu_data()[lossmat_lstm_.offset(0,0,i,j)]
					= (Dtype)0.;
			}

		}
	}
	// 4) Accumulate the loss values
	Dtype loss = 
		caffe_cpu_asum<Dtype>(lossmat_cnn_.count(), lossmat_cnn_.cpu_data())
		+ caffe_cpu_asum<Dtype>(lossmat_lstm_.count(), lossmat_lstm_.cpu_data());
	// Send out loss value
	top[0]->mutable_cpu_data()[0] = loss;
}

                                                                    // Backward

template <typename Dtype>
void PairwiseRankingLossLayer<Dtype>::Backward_cpu(
		const vector<Blob<Dtype>*>& top,
		const vector<bool>& propagate_down,
		const vector<Blob<Dtype>*>& bottom) {

	// FIXME: Parallelize with OpenMP!

	int batchsize = bottom[0]->num();

	// helper function, C++ feature: lambda expression
	Dtype Tokenxvvk = [](Dtype margin, Dtype* x, Dtype* v, Dtype* vk,
			size_t sz) {
		return margin - caffe_cpu_dot<Dtype>(sz, x, v) + caffe_cpu_dot<Dtype>(sz, x, vk);
	}
	Dtype Tokenvxxk = [](Dtype margin, Dtype* v, Dtype* x, Dtype* xk,
			size_t sz) {
		return margin - caffe_cpu_dot<Dtype>(sz, v, x) + caffe_cpu_dot<Dtype>(sz, v, xk);
	}

	// calculate gradient of loss w.r.t each x_i and v_i
	for (int i = 0; i < batchsize; i++) {
		// pebpxi[:] = 0, pebpvi[:] = 0
		caffe_set(pebpxi_.count(), (Dtype)0., pebpxi_.mutable_cpu_data());
		caffe_set(pebpvi_.count(), (Dtype)0., pebpvi_.mutable_cpu_data());

		// calculate gradient
		for (int j = 0; j < batchsize; j++) {
			if (i==j) continue;

			// ___ pebpxi ___

			// token xi vi vj
			if ( Tokenxvvk(margin_,
					// x_i
					bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
					// v_i
					bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
					// v_j
					bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
					// size
					bottom[0]->width() )>(Dtype)0.) {
				// pebpxi:add( vj ):add( -vi )
				caffe_add<Dtype>(bottom[0]->width(),
						bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
						pebpxi_.cpu_data(),
						pebpxi_.mutable_cpu_data());
				caffe_sub<Dtype>(bottom[0]->width(),
						pebpxi_.cpu_data(),
						bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
						pebpxi_.mutable_cpu_data());
			}
			// token vi xi xj
			if ( Tokenvxxk(margin_,
					// v_i
					bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
					// x_i
					bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
					// x_j
					bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
					// size
					bottom[0]->width() )>(Dtype)0.) {
				// pebpxi:add( -vi )
				caffe_sub<Dtype>(bottom[0]->width(),
						pebpxi_.cpu_data(),
						bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
						pebpvi_.mutable_cpu_data());
			}
			// token vj xj xi
			if ( Tokenvxxk(margin_,
					// vj
					bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
					// xj
					bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
					// xi
					bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
					// size
					bottom[0]->width())>(Dtype)0.) {
				// pebpxi:add( vj )
				caffe_add<Dtype>(bottom[0]->width(),
						pebpxi_.cpu_data(),
						bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
						pebpxi_.mutable_cpu_data());
			}
			
			// ___ pebpvi ___
			
			// token vi xi xj
			if ( Tokenvxxk(margin_,
					// v_i
					bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
					// x_i
					bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
					// x_j
					bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
					// size
					bottom[0]->width() )>(Dtype)0.) {
				// pebpvi:add( xj ):add( -xi )
				caffe_add<Dtype>(bottom[0]->width(),
						bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
						pebpvi_.cpu_data(),
						pebpvi_.mutable_cpu_data());
				caffe_sub<Dtype>(bottom[0]->width(),
						pebpvi_.cpu_data(),
						bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
						pebpvi_.mutable_cpu_data());
			}
			// token xi vi vj
			if ( Tokenvxxk(margin_,
					// x_i
					bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
					// v_i
					bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
					// v_j
					bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
					// size
					bottom[0]->width() )>(Dtype)0.) {
				// pebpvi:add( -xi )
				caffe_sub<Dtype>(bottom[0]->width(),
						pebpvi_.cpu_data(),
						bottom[0]->cpu_data()+bottom[0]->offset(i,0,0,0),
						pebpvi_.mutable_cpu_data());
			}
			// token xj vj vi
			if ( Tokenvxxk(margin_,
					// xj
					bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
					// vj
					bottom[1]->cpu_data()+bottom[1]->offset(j,0,0,0),
					// vi
					bottom[1]->cpu_data()+bottom[1]->offset(i,0,0,0),
					// size
					bottom[0]->width())>(Dtype)0.) {
				// pebpvi:add( xj )
				caffe_add<Dtype>(bottom[0]->width(),
						pebpvi_.cpu_data(),
						bottom[0]->cpu_data()+bottom[0]->offset(j,0,0,0),
						pebpvi_.mutable_cpu_data());
			}
		} // end for j in range(batchsize)

		// Send out pebpxi and pebpvi
		caffe_copy<Dtype>(pebpxi_.count(),
				pebpxi_.cpu_data(),
				bottom[0]->mutable_cpu_diff()+bottom[0]->offset(i,0,0,0));
		caffe_copy<Dtype>(pebpvi_.count(),
				pebpvi_.cpu_data(),
				bottom[1]->mutable_cpu_diff()+bottom[0]->offset(i,0,0,0));
	}
}

} // namespace caffe
