'''
FlashLight: Personal PyTorch Helpers
'''
from typing import *
import sys, os, re, _io
import pickle
import ujson as json  # Fastest Json
from collections import Counter, defaultdict
import string
from pprint import pprint
import gzip, zstd
import unittest

import nltk
import numpy as np
import torch as th


def jsonSave(obj: object, dest: Any) -> None:
    '''
    Serilize an object composed of List and Dict into file specified by
    path or io wrapper.
    '''
    if isinstance(dest, str):
        gz, zst = dest.endswith('.gz'), dest.endswith('.zst')
        if zst:
            with open(dest, 'wb') as f:
                f.write(zstd.dumps(json.dumps(obj).encode()))
        else:
            with (gzip.open(dest, 'wb') if gz else open(dest, 'w')) as f:
                f.write(json.dumps(obj).encode() if gz else json.dumps(obj))
    else:
        json.dump(obj, dest)


def jsonLoad(src: Any) -> Any:
    '''
    Load object from Json file.
    '''
    if isinstance(src, str):
        gz, zst = src.endswith('.gz'), src.endswith('.zst')
        if zst:
            with open(src, 'rb') as f:
                return json.loads(zstd.loads(f.read()))
        else:
            with (gzip.open(src, 'rb') if gz else open(src, 'r')) as f:
                return json.loads(f.read())
    else:
        return json.load(src)


class TestJson(unittest.TestCase):
    def test_json(self, fpath='/tmp/xxx'):
        jsonSave([fpath], fpath)
        self.assertTrue(fpath == jsonLoad(fpath)[0])
        os.remove(fpath)
    def test_json_gz(self, fpath='/tmp/xxx.gz'):
        jsonSave([fpath], fpath)
        self.assertTrue(fpath == jsonLoad(fpath)[0])
        os.remove(fpath)
    def test_json_zst(self, fpath='/tmp/xxx.zst'):
        jsonSave([fpath], fpath)
        self.assertTrue(fpath == jsonLoad(fpath)[0])
        os.remove(fpath)
    def test_json_f(self, fpath='/tmp/xxx'):
        with open(fpath, 'w') as f:
            jsonSave([fpath], f)
        with open(fpath, 'r') as f:
            self.assertTrue(fpath == jsonLoad(f)[0])
        os.remove(fpath)


def pklSave(obj: object, fpath: str) -> None:
    '''
    dump object to a file
    '''
    if isinstance(fpath, str):
        with open(fpath, 'wb') as f:
            pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)
    else:  # _io.* wrapper
        pickle.dump(obj, fpath, protocol=pickle.HIGHEST_PROTOCOL)


def pklLoad(fpath: str) -> object:
    '''
    load object from file
    '''
    if isinstance(fpath, str):
        with open(fpath, 'rb') as f:
            return pickle.load(f)
    else:  # _io.* wrapper
        return pickle.load(fpath)


class TestPkl(unittest.TestCase):
    def test_pkl(self, fpath='/tmp/xxx.pkl'):
        pklSave([fpath], fpath)
        self.assertTrue(fpath == pklLoad(fpath)[0])
        os.remove(fpath)
    def test_pkl_f(self, fpath='/tmp/xxx.pkl'):
        with open(fpath, 'wb') as f:
            pklSave([fpath], f)
        with open(fpath, 'rb') as f:
            self.assertTrue(fpath == pklLoad(f)[0])
        os.remove(fpath)


def tokenize(s: str) -> List[str]:
    '''
    Turn a raw sentence into a list of tokens.
    '''
    tok = re.sub(f'[{string.punctuation}]', ' ', s)  # remove punctuation
    tok = ' '.join(tok.lower().split())  # lower and reformat
    tok = nltk.word_tokenize(tok)  # tokenize
    return tok


class Vocabulary(object):
    '''
    Load vocabulary from word counter, and do the i->w / w->i mapping
    '''
    def __init__(self, ctr: Counter, threshold = 5):
        self.vocab = {}
        self.vocablist = ['<padding>', '<start>', '<end>', '<unknown>'] # 0 1 2 3
        self.vocablist.extend(sorted(k for k, v in ctr.items() if v >= threshold))
        for (i, w) in enumerate(self.vocablist):
            self.vocab[int(i)] = str(w)
            self.vocab[str(w)] = int(i)
        self.threshold = threshold
    def __len__(self):
        return len(self.vocablist)
    def __getitem__(self, index):
        if isinstance(index, list):
            return [self.__getitem__(x) for x in index]
        elif isinstance(index, str):
            return self.vocab.get(index, 3)  # <unknown>
        elif isinstance(index, int):
            return self.vocab.get(index, '<unknown>')
        else:
            raise TypeError(f"{type(index)}: {index}")
    def __call__(self, index):
        return self.__getitem__(index)
    def get(self, index, fallback):
        return self.vocab.get(index, fallback)


def padLLI(lli: List[List[int]], padding = 0) -> (np.array, List[int]):
    '''
    Pad a list of lists of integers with zero. The lenghts of lists may vary.
    a numpy.array with shape (num_lists, maxlen) will be returned.
    '''
    lens = list(map(len, lli))
    pdli = []
    for j, li in enumerate(lli):
        pdli.append( list(lli[j]) + [padding] * (max(lens) - len(li)) )
    return np.array(pdli), lens
