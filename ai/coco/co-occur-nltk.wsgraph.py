import pickle
from collections import Counter
import numpy as np
from pprint import pprint
import copy
import re
import pygraphviz as gv
from subprocess import call
import json
import os

def printR(*args):
    '''
    print in red color
    '''
    print(' '.join(('\033[31;1m', *(str(x) for x in args), '\033[m')))

def print_(color, *args):
    '''
    generalized color print
    '''
    print(' '.join((f'\033[{color}m', *(str(x) for x in args), '\033[m')))

def intersectionN(sa, sb):
    return len(set(sa).intersection(set(sb)))

def translateIdx(l_imid, cluster):
    clusterTrans = []
    for clu in cluster:
        clusterTrans.append(set(map(lambda x: l_imid[x], clu)))
    return clusterTrans

def dumpClustersHL(clusters) -> None:
    '''
    dump clusters, with items whose refcount > 1 being highlighted
    '''
    ctr = Counter()
    for cluster in clusters:
        ctr.update(cluster)
    strcls = str(clusters)
    for k, v in ctr.items():
        if v > 1:
            strcls = re.sub(str(k), '\033[35;1m'+str(k)+'\033[m', strcls)
    print(strcls)

def main():

    with open('co-occur-nltk.py.ws_byimg.pkl', 'rb') as f:
        ws_byimg = pickle.load(f)

    # Reduce dataset size for DEBUG
    ws_byimg = {k: v for i, (k, v) in enumerate(ws_byimg.items()) if i < 100}
    comat = np.ndarray((len(ws_byimg), len(ws_byimg)), dtype=np.uint8)
    l_image_id = sorted(list(ws_byimg.keys()))
    for i, ik in enumerate(l_image_id):
        for j, jk in enumerate(l_image_id):
            comat[i,j] = intersectionN(ws_byimg[ik], ws_byimg[jk])
    np.fill_diagonal(comat, 0)
    print(comat)
    print(comat.shape, comat.max(), comat.min(), comat.mean(), np.median(comat))

    printR('> found', len(ws_byimg), 'word sets')

    # draw graph of the comat
    if True:
        printR('> drawing grpah for the comat')
        G = gv.AGraph(ranksep=1.2, mindist=2.0, K=0.9, defaultdist=2)
        for idx in np.argwhere(comat >= 3):
            G.add_node(idx[0], label='', shape='box')
            G.add_node(idx[1], label='', shape='box')
            G.add_edge(idx[0], idx[1], label=comat[idx[0], idx[1]],
                    color='gray', penwidth=4*comat[idx[0], idx[1]],
                    len=3, minlen=2)
        G.layout()
        G.draw('x.png')

        printR('> refine the graph with real image')
        with open('annotations/captions_val2014.json', 'r') as f:
            json_orig = json.load(f)
        for n in G.nodes():
            image_id = l_image_id[int(n.name)]
            print('node', n, image_id)
            call(['python3', 'cocofetch.py', 'annotations/captions_val2014.json', str(image_id)])
            imfname = [x for x in json_orig['images'] if x['id'] == image_id][0]['file_name']
            imfpath = os.path.join('pool', imfname)
            n.attr['image'] = imfpath
            n.attr['fixedsize'] = 'true'
            n.attr['imagescale'] = 'both'
            n.attr['height'] = 1.5
            n.attr['width'] = 1.5
        G.layout()
        G.draw('xp.png')

    exit()

    # merging
    Clusters = {} # all history of clusters
    clusters = []
    for nround, i in enumerate(range(comat.max(), 3, -1)):
        printR(f'> Merging round {nround}, processing links with weight {i}')
        idxs = np.argwhere(comat == i)
        #print_('33;1', idxs)
        printR(f'  > found {len(idxs)} links')
        for idx in idxs:
            x, y = idx
            xtrans, ytrans = l_image_id[x], l_image_id[y] # translated index
            #print('inserting', idx, xtrans, ytrans, 'to clusters') # DEBUG
            #print('intersection', ws_byimg[xtrans].intersection(ws_byimg[ytrans]))
            if len(clusters) == 0:
                clusters.append(set([x, y]))
            else:
                if any(x in s or y in s for s in clusters):
                    for n in range(len(clusters)):
                        if x in clusters[n] or y in clusters[n]:
                            clusters[n].add(x)
                            clusters[n].add(y)
                else:
                    clusters.append(set([x, y]))
        Clusters[i] = copy.deepcopy(clusters)
        print_('36;1', clusters)
        print(translateIdx(l_image_id, clusters))

        # do reference count
        ctr = Counter()
        for cluster in clusters:
            ctr.update(cluster)
        print(ctr)

        dumpClustersHL(clusters)
        dumpClustersHL(translateIdx(l_image_id, clusters))

    printR('> generated {} clusters in total'.format(len(clusters)))
    print_('36;1', clusters)

    # complete the cluster
    for i in range(comat.shape[0]):
        if all(i not in s for s in clusters):
            clusters.append(set((i,)))
    printR('> final round: {} clusters'.format(len(clusters)))
    print_('36;1', clusters)

main()
