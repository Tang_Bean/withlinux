import json
import sys
import nltk
import re
import os
from pprint import pprint
from collections import Counter, defaultdict
from functools import partial
import pickle
import plotly
import string
from statistics import mean
from subprocess import call
import numpy as np

'''
https://www.quora.com/How-do-I-remove-stopwords-from-a-file-using-python
'''

POS_TAG = True
COMAT = True


def red(*_args) -> str:
    '''
    colorize a string in red color
    '''
    return ' '.join(['\033[31;1m', *(str(x) for x in _args), '\033[m'])


def yellow(*_args) -> str:
    return '\033[33;1m' + ' '.join(str(x) for x in _args) + '\033[m'


def getNumImages(_json) -> int:
    '''
    _json: raw COCO json file
    return: int, number of images
    '''
    return len(_json['images'])


def intersect(_seta, _setb) -> int:
    return len(set(_seta).intersection(set(_setb)))


class RotBar(object):
    def __init__(self):
        self.bars = '-\\|/'
        self.state = 0

    def get(self):
        bar = self.bars[self.state % len(self.bars)]
        self.state += 1
        return bar


rotBar = RotBar()


def makeBarPlot():
    with open(f'{__file__}.coo_stat.pkl', 'rb') as f:
        ctr = pickle.load(f)
    total = sum(x for x in ctr.values())
    average = mean([x*y for x, y in ctr.items()]) / total
    print('min value', min(x for x in ctr.keys()))
    print('max value', max(x for x in ctr.keys()))
    print('mean value', average)
    accu = 0.0
    for k, v in sorted(ctr.items(), key=lambda x: x[0]):
        accu += v/total
        print(k, v/total, accu)
    del accu

    exit()
    x, y = zip(*ctr.items())
    plotly.offline.plot({
        'data': [plotly.graph_objs.Bar(x=x, y=y)],
    }, image='svg')


def visualize():
    with open(f'{__file__}.ws_byimg.pkl', 'rb') as f:
        ws = pickle.load(f)
    with open(sys.argv[1], 'r') as f:
        json_orig = json.load(f)
    for k, v in ws.items():
        print('\0337\033[K', red('> Searching ... {}'.format(rotBar.get())), end='\0338')
        sys.stdout.flush()
        irow = [intersect(v, vv) for kk, vv in ws.items() if k != kk]
        iidx = [kk for kk, vv in ws.items() if k != kk]

        colimit = 10
        if any(x == colimit for x in irow):
            idx = [x == colimit for x in irow].index(True)
            idx = iidx[idx]
            print(red('> image_id pair'), k, idx)
            print(red('ws A'), ws[k])
            pprint(list(x['caption']
                        for x in json_orig['annotations'] if x['image_id'] == k))
            print(red('ws B'), ws[idx])
            pprint(list(x['caption']
                        for x in json_orig['annotations'] if x['image_id'] == idx))
            print(red('intersection'), set(ws[k]).intersection(set(ws[idx])))
            # Such usage of pos_tag is problematic but runs fast. FIXME
            pprint(nltk.pos_tag(set(ws[k]).intersection(set(ws[idx]))))
            infoA = list(x for x in json_orig['images'] if x['id'] == k)[0]
            infoB = list(x for x in json_orig['images'] if x['id'] == idx)[0]
            print(red('image info A'))
            pprint(infoA)
            print(red('image info B'))
            pprint(infoB)

            cmd = ['wget', '--no-verbose', '-c']
            print('downloading', infoA['flickr_url'])
            call(cmd + [infoA['flickr_url']])
            print(os.path.join(os.getcwd(),
                               os.path.basename(infoA['flickr_url'])))
            print('downloading', infoB['flickr_url'])
            call(cmd + [infoB['flickr_url']])
            print(os.path.join(os.getcwd(),
                               os.path.basename(infoB['flickr_url'])))

            ans = input(yellow('Do you want the next sample (y/n)? '))
            if 'n' == ans:
                exit()


class COOC(object):
    '''
    much faster replacement to raw co-occurrence matrix, if pickled.
    '''
    def __init__(self, wordsets_byimgid):
        self.imageids = sorted(list(wordsets_byimgid.keys()))
        self.ws_byimg = wordsets_byimgid
    def __len__(self):
        return len(self.ws_byimg)
    def __getitem__(self, index):
        '''
        index must be a tuple, and meet the range requirement
        '''
        assert(len(index) == 2)
        i, j = index
        assert(i in self.ws_byimg and j in self.ws_byimg) # valid image_number?
        return len(set(self.ws_byimg[i]).intersection(set(self.ws_byimg[j])))
    def test(self):
        from random import choice
        for targetval in range(10):
            while True:
                i, j = choice(self.imageids), choice(self.imageids)
                wsc = self[(i, j)]
                if wsc == targetval:
                    print(' -', i, j, wsc)
                    print('  ', self.ws_byimg[i], self.ws_byimg[j])
                    break


def main():

    if os.path.exists(f'{__file__}.coo_stat.pkl'):
        ans = input(
            'stat already exists. plot or visualize, or ignore (p/v/i)? ')
        if ans == 'p':
            makeBarPlot()
            exit()
        elif ans == 'v':
            visualize()
            exit()
        else:  # 'i'
            pass

    assert(len(sys.argv) >= 2)
    with open(sys.argv[1], 'r') as f:
        json_orig = json.load(f)

    annotations = json_orig['annotations']
    #annotations = annotations[:100] # FOR DEBUG USE
    print(red('> found', len(annotations), 'annotations for',
              getNumImages(json_orig), 'images'))

    print(red('> collect sents per img'))
    sents_byimg = defaultdict(list)
    for i, each in enumerate(annotations, 1):
        re_punkt = re.compile(f'[{string.punctuation}]')
        cleaned_caption = re_punkt.sub(' ', each['caption'].lower())
        cleaned_caption = ' '.join(cleaned_caption.split())
        sents_byimg[each['image_id']].append(cleaned_caption)
    pprint(sents_byimg[list(sents_byimg.keys())[0]])  # show example

    print(red('> tokenize sents per img'))
    toks_byimg = defaultdict(list)
    for i, eachimg in enumerate(sents_byimg.items(), 1):
        k, v = eachimg
        for eachsent in v:
            toks_byimg[k].append(nltk.word_tokenize(eachsent))
        print('\0337\033[K>', i, getNumImages(json_orig), '\0338', end='')
        sys.stdout.flush()
    pprint(toks_byimg[list(toks_byimg.keys())[0]])

    if POS_TAG:
        # Analyse POS tag and only keep NN* words in the set
        print(red('> POS tagging ...'))
        tagged_byimg = defaultdict(list)
        for i, eachimg in enumerate(toks_byimg.items(), 1):
            k, v = eachimg
            tagged_byimg[k] = nltk.pos_tag_sents(v)
            print('\0337\033[K>', i, len(toks_byimg), '\0338', end='')
            sys.stdout.flush()
        pprint(tagged_byimg[list(tagged_byimg.keys())[0]])

        print(red('> generate NN* word sets per img'))
        ws_byimg = defaultdict(set)
        for k, vs in tagged_byimg.items():
            for v in vs:
                for w, t in v:
                    if 'NN' in t:
                        ws_byimg[k].add(w)
        pprint(ws_byimg[list(ws_byimg.keys())[0]])
    else:
        # Keep all non-stopwords in the set
        print(red('> generate word sets per img'))
        ws_byimg = defaultdict(set)
        for k, vs in toks_byimg.items():
            for v in vs:
                for w in v:
                    ws_byimg[k].add(w)
        pprint(ws_byimg[list(ws_byimg.keys())[0]])

    print(red('> remove stopwords from word sets'))
    stopwords = set(nltk.corpus.stopwords.words('english'))
    print(stopwords)
    removal_candiates = set(list(stopwords) + ['.', ','])
    # print(stopwords)
    for k in ws_byimg.keys():
        ws_byimg[k] -= removal_candiates
    pprint(ws_byimg[list(ws_byimg.keys())[0]])

    print(red('> making vocabulary'))
    vocab = Counter()
    for k, v in ws_byimg.items():
        vocab.update(v)
    vocab = sorted(list(vocab.keys()))
    print(vocab)

    print(red('> save visualization info e.g. ws_byimg'))
    with open(f'{__file__}.ws_byimg.pkl', 'wb') as f:
        pickle.dump(ws_byimg, f, protocol=0)

    print(red('> save fast version of picle COOC matrix'))
    cooc = COOC(ws_byimg)
    with open(f'{__file__}.cooc.pkl', 'wb') as f:
        pickle.dump(cooc, f, protocol=pickle.HIGHEST_PROTOCOL)
    cooc.test()

    print(red('> co-occur matrix and stat'))
    coo_stat = Counter()
    l_image_id = sorted(list(ws_byimg.keys()))
    if COMAT:
        co_mat = np.ndarray((len(ws_byimg), len(ws_byimg)), dtype=np.uint8)
    for i, id_i in enumerate(l_image_id):
        print('\0337\033[K>', i+1, '/', getNumImages(json_orig), '\0338', end='')
        sys.stdout.flush()
        irow = list(map(
            partial(intersect, ws_byimg[id_i]),
            [ws_byimg[id_j] for j, id_j in enumerate(l_image_id) if j != i]))
        coo_stat.update(irow)
        if COMAT:
            irow.insert(i, len(ws_byimg[id_i]))
            co_mat[i, :] = irow
    pprint(coo_stat)

    print(red('> saving stat data'))
    with open(f'{__file__}.coo_stat.pkl', 'wb') as f:
        pickle.dump(coo_stat, f, protocol=0)
    if COMAT:
        print(red('> saving co mat'))
        np.savez(f'{__file__}.comat.npz', co_mat)


main()
