# For pypy compatibility
USE_NUMPY = False
USE_PLOTLY = False

import json
import sys
from collections import defaultdict
from collections import Counter
from pprint import pprint
from multiprocessing.dummy import Pool
from functools import partial
import pickle

if USE_NUMPY:
    import numpy as np
if USE_PLOTLY:
    import plotly

assert(len(sys.argv) >= 2)

with open(sys.argv[1], 'r') as f:
    j = json.load(f)
json_orig = j

'''
annotations: List[Dict{image_id: int, id: int, caption: str}]
'''
annotations = j['annotations']
print(f"found {len(j['images'])} images")
num_images = len(j['images'])
print(f'found {len(annotations)} annotations')

sents_perimg = defaultdict(list) # id: int -> list[sent: str]
for i, each in enumerate(annotations, 1):
    #print(each)
    sents_perimg[each['image_id']].append(each['caption'])
#pprint(sents_perimg)

'''
list of sentences to word pool
'''
def lsent2wpool(_l):
    _wp = set()
    for eachsent in _l:
        sent = eachsent.lower().strip().replace('\.', '')
        for w in sent.split():
            _wp.add(w)
    return sorted(list(_wp))

wp_perimg = defaultdict(list) # id: int -> list[words: str]
for i, eachpair in enumerate(sents_perimg.items(), 1):
    image_id, caption = eachpair
    wp_perimg[image_id] = lsent2wpool(caption)
#pprint(wp_perimg)
assert(len(wp_perimg.keys()) == num_images)

'''
figure out the word co-occurence matrix
'''
def cooccur(_la, _lb) -> int:
    return len(set(_la).intersection(set(_lb)))

print(f'matrix size {num_images} ^2 = ', num_images ** 2)
#cooccur_mat = np.ndarray((num_images, num_images), dtype=np.uint8)
cooccur_ctr = Counter()
l_image_id = sorted(list(wp_perimg.keys())) # used as matrix row-col index

with Pool(2) as threadpool:
    for i, iid in enumerate(l_image_id):
        print(f'\0337\033[K  {i} / {num_images} ({(i*100/num_images):.1f}%)\0338', end='')
        sys.stdout.flush()
        #for j, jid in enumerate(l_image_id):
        #    coo = cooccur(wp_perimg[iid], wp_perimg[jid]) # XXX: too slow
        #    cooccur_mat[i][j] = coo
        #    #print(coo, ' ', end='')
        irow = list(threadpool.map(partial(cooccur, wp_perimg[iid]),
            [wp_perimg[jid] for jid in l_image_id] )) # faster
        #print(irow)
        ### For visualization
        ###xlimit = 20
        ###if any(x == xlimit for x in irow[0:i]+irow[i+1:]):
        ###    j = [x == xlimit for x in irow].index(True)
        ###    if j == i: j = [x == xlimit for x in irow].index(True, i+1)
        ###    jid = l_image_id[j]
        ###    print('\n', i, iid, j, jid, irow[j])
        ###    print('wp of i', wp_perimg[iid])
        ###    print('wp of j', wp_perimg[jid])
        ###    print('intersection',
        ###            set(wp_perimg[iid]).intersection(set(wp_perimg[jid])))
        ###    print('sents i', sents_perimg[iid])
        ###    print('sents j', sents_perimg[jid])
        ###    imageinfo_i = [x for x in json_orig['images'] if x['id'] == iid]
        ###    imageinfo_j = [x for x in json_orig['images'] if x['id'] == jid]
        ###    pprint(imageinfo_i)
        ###    pprint(imageinfo_j)
        ###    # look for the next one?
        ###    if input('Continue (Y/n)?') == 'n': break
        cooccur_ctr.update(irow)
        #cooccur_mat[i][:] = irow
#exit() ### for visualization
with open('co-occur-ctr.txt', 'wb') as f:
    pickle.dump(cooccur_ctr, f, protocol=0)

'''
save the matrix
'''
#comatfname = f'{__file__}.comat.npz' if len(sys.argv <= 2) else sys.argv[2]
#np.savez(comatfname, cooccur_mat)

# post analysis
#with open('./co-occur-ctr.txt', 'rb') as f:
#    ctr = pickle.load(f)
if USE_PLOTLY:
    x, y = zip(*cooccur_ctr.items())
    plotly.offline.plot({
        'data': [plotly.graph_objs.Bar(x=x,y=y)],
    }, image='svg')
