'''
Image Captioning -- Ranking -- GRU/LSTM
:ref: https://arxiv.org/abs/1707.05612
:ref: https://github.com/fartashf/vsepp
'''
import pickle
import json
import sys, os, re
import argparse
import pickle
from pprint import pprint
from functools import reduce
import random
import subprocess
from typing import *
import shutil, shlex
import torch as th
from torch.utils.data import Dataset, DataLoader, ConcatDataset
from torch.utils.data.dataset import Subset
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import numpy as np
import pylab as lab
import tqdm
import tensorboardX as TBX
import time
import json

import lingual
import visual


def systemShell(command: List[str]) -> str:
    '''
    Execute the given command in system shell. Unlike os.system(), the program
    output to stdout and stderr will be returned.
    '''
    result = subprocess.Popen(command, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE).communicate()[0].decode().strip()
    return result


def featstat(feat):
    return ' '.join([f'mean {feat.mean().item():.5f}',
        f'var {feat.var().item():.5f}', f'min {feat.min().item():.5f}',
        f'max {feat.max().item():.5f}'  ])


def makeVariable(tensor, use_cuda = False):
    if isinstance(tensor, np.ndarray):
        t = th.autograd.Variable(th.from_numpy(tensor), requires_grad=False)
    elif isinstance(tensor, th.Tensor):
        t = th.autograd.Variable(tensor, requires_grad=False)
    else:
        raise TypeError
    return t.cuda() if use_cuda else t.cpu()


def adjustLearningRate(optim, lr0, eph, giter) -> float:
    '''
    Adjust the learning rate of the given optimizer. Method: step
    '''
    lr = lr0 * (0.1 ** (eph // 15))
    for param_group in optim.param_groups:
        param_group['lr'] = lr
    return lr


class PairwiseRankingLoss(th.nn.Module):
    def __init__(self, margin=0, max_violation=False):
        super(PairwiseRankingLoss, self).__init__()
        self.margin = margin
        self.max_violation = max_violation
    def forward(self, xs, vs):
        scores = th.mm(xs, vs.t())
        diagonal = scores.diag().view(xs.size(0), 1)
        diag  = diagonal.expand_as(scores)
        diagT = diagonal.t().expand_as(scores)
        cost_x2v = (self.margin + scores - diag).clamp(min=0)
        cost_v2x = (self.margin + scores - diagT).clamp(min=0)

        # clear diagonals
        eye = th.autograd.Variable(th.eye(scores.size(0))) > .5
        eye = eye.cuda() if scores.is_cuda else eye
        cost_x2v = cost_x2v.masked_fill_(eye, 0)
        cost_v2x = cost_v2x.masked_fill_(eye, 0)

        # keep the maximum violating negative for each query
        if self.max_violation:
            cost_x2v = cost_x2v.max(1)[0]
            cost_v2x = cost_v2x.max(0)[0]

        return cost_x2v.sum() + cost_v2x.sum()



def getRecall(scores: np.ndarray, ks: List[int] = (1,5,10,50)):
    '''
    caculate the recall value
    '''
    assert(len(scores.shape) == 2)
    queries, candidates = scores.shape[0], scores.shape[1]
    ranks = [list(np.argsort(scores[i]).flatten()[::-1])
             for i in range(queries)]
    recalls = [ranks[i].index(i) for i in range(queries)]
    r_mean, r_med = np.mean(recalls), np.median(recalls)
    r_ks = []
    for k in ks:
        catch = np.sum(x < k for x in recalls)
        r_ks.append((k, 100* catch / candidates))
    recall_score = sum(x[1] for x in r_ks)
    recalls = [('mean', r_mean+1), ('med', r_med+1)]
    recalls.extend(r_ks)
    recall_raw = recalls
    recalls = [(f'r@{k}', f'{r:>5.1f}') for (k, r) in recalls]
    recalls = ',  '.join(' '.join(t) for t in recalls)
    return recalls, recall_score, recall_raw


class CocoPreproDataset(Dataset):
    def __init__(self, cnnpkl = './coco.all.vgg19', tokspkl = './coco.all.toks'):
        self.lingual = lingual.CocoLtokDataset(tokspkl)
        self.visual  = visual.CocoVRepDataset(cnnpkl)
    def __len__(self):
        return len(self.lingual)
    def __getitem__(self, index):
        tok, iid, sid = self.lingual[index]
        imagerep = self.visual.byiid(iid)
        return imagerep, tok, iid, sid
    def getCollateFun(self):
        def _collate(batch):
            imagereps, toks, iids, sids = zip(*batch)
            idxs = np.argsort([len(x) for x in toks]).flatten()[::-1]
            idxs = list(x for x in idxs)  # Stupid negative strides.
            toks = [toks[i] for i in idxs]
            iids = [iids[i] for i in idxs]
            sids = [sids[i] for i in idxs]
            imagereps = th.stack(imagereps, dim=0)[idxs]
            return imagereps, list(toks), list(iids), list(sids)
        return _collate


class JointEmbNet(th.nn.Module):
    '''
    Image-Text Joint Embedding Model
    '''
    def __init__(self, dimvocab, *,
                 dimemb=1024, dimcnn=4096, dimw2v=300,
                 rnntype = 'GRU'):
        super(JointEmbNet, self).__init__()
        self.rnntype = rnntype
        self.encoder = th.nn.Embedding(dimvocab, dimw2v)
        self.rnn = getattr(th.nn, rnntype)(dimw2v, dimemb)
        self.cnnaffine = th.nn.Linear(dimcnn, dimemb)

        # Xavier for cnnaffine
        r = np.sqrt(6.) / np.sqrt(dimcnn + dimemb)
        self.cnnaffine.weight.data.uniform_(-r, r)
        self.cnnaffine.bias.data.fill_(0.)

        # Uniform for encoder
        self.encoder.weight.data.uniform_(-0.1, 0.1)

    def forwardLingual(self, toks):
        '''
        Forward the lingual part
        '''
        ptoks, lens = padLLI(toks)
        ptoks = makeVariable(ptoks, True)
        wordembs = self.encoder(ptoks.t())
        pack = pack_padded_sequence(wordembs, lens)
        if 'LSTM' == self.rnntype:
            out, (hn, cn) = self.rnn(pack)
        else:  # GRU and RNN
            out, hn = self.rnn(pack)
        #unpack, _ = pad_packed_sequence(out)
        #hnp = unpack[[x-1 for x in lens], range(len(lens)), :].squeeze()
        vs = hn.squeeze()
        #print('error', (hnp - vs).norm())  # identical
        if len(vs.shape) == 2:
            vs = th.nn.functional.normalize(vs, dim=1) # MUST
        else:
            vs = th.nn.functional.normalize(vs, dim=0) # MUST
        return vs

    def forwardVisualPre(self, cnnfeat):
        '''
        Forward the pre-calculated visual part
        '''
        cnnfeat = makeVariable(cnnfeat, True)
        xs = th.nn.functional.relu(cnnfeat)
        xs = th.nn.functional.normalize(xs)
        xs = self.cnnaffine(xs)
        xs = th.nn.functional.normalize(xs, dim=1) # MUST
        return xs

    def forward(self, cnnfeat, toks, iids, sids):
        xs = self.forwardVisualPre(cnnfeat)
        vs = self.forwardLingual(toks)
        return xs, vs


def evaluation(valset, model, snapshot=False,
        *, best=[0.], tbx = False, giter = 0):
    print('>> VALIDATION')
    cnnfeats, rnnfeats = [], []
    model.eval()
    for iteration in tqdm.tqdm(range(len(valset))):
        xs, vs, iid, sid = valset[iteration]
        xs = xs.unsqueeze(0)
        xs, vs = model(xs, [vs], [iid], [sid])

        cnnfeats.append(xs.detach())
        rnnfeats.append(vs.detach())

    cnnfeats, rnnfeats = th.cat(cnnfeats), th.stack(rnnfeats)
    print(' * Validation set shape', cnnfeats.shape, rnnfeats.shape)
    print(' * Dump CNN feats', featstat(cnnfeats))
    print(' * Dump RNN feats', featstat(rnnfeats))
    scores = cnnfeats.mm(rnnfeats.t()).cpu().numpy()
    recalls, pt, ptraw = getRecall(scores)
    print(' * Recall(x->v):', recalls)
    recallsT, ptT, ptTraw = getRecall(scores.T)
    print(' * Recall(v->x):', recallsT)

    if tbx:
        #tbx.add_scalar('validate/loss',        loss.item(), giter)
        tbx.add_scalar('validate/recall.mean', ptraw[0][1], giter)
        tbx.add_scalar('validate/recall.med',  ptraw[1][1], giter)
        tbx.add_scalar('validate/recall.1',    ptraw[2][1], giter)
        tbx.add_scalar('validate/recall.5',    ptraw[3][1], giter)
        tbx.add_scalar('validate/recall.10',   ptraw[4][1], giter)
        tbx.add_scalar('validate/recallT.mean', ptTraw[0][1], giter)
        tbx.add_scalar('validate/recallT.med',  ptTraw[1][1], giter)
        tbx.add_scalar('validate/recallT.1',    ptTraw[2][1], giter)
        tbx.add_scalar('validate/recallT.5',    ptTraw[3][1], giter)
        tbx.add_scalar('validate/recallT.10',   ptTraw[4][1], giter)

    # save the model
    if snapshot:
        th.save([model.state_dict(), pt+ptT, recalls, recallsT], snapshot)
        print('   - current score', pt + ptT, 'while the best is', best[0])
    if snapshot and pt + ptT > best[0]:
        best[0] = pt + ptT
        shutil.copyfile(snapshot,
                os.path.join(os.path.dirname(snapshot), 'model_best.pth'))
        print('   - saving cnnfeats and rnnfeats from the best model')
        th.save([cnnfeats.detach().cpu(), rnnfeats.detach().cpu()],
                os.path.join(os.path.dirname(snapshot), 'feat_best.pth'))
        if tbx:
            tbx.add_text('best/model-update-iter', str(giter), giter)
            tbx.add_text('best/recall-score', str(pt+ptT), giter)
            tbx.add_text('best/recall-x2v', recalls, giter)
            tbx.add_text('best/recall-v2x', recallsT, giter)


def main_train(ag):

    # create log directory
    if not os.path.exists(ag.logdir):
        os.system(f'mkdir -p {ag.logdir}')
    tbx = TBX.SummaryWriter(ag.logdir)

    print('> initialize dataloader')
    cocodataset = CocoPreproDataset(ag.cnnpkl, ag.tokspkl)
    valset = Subset(cocodataset, range(1000))
    trainset = Subset(cocodataset, range(1000, len(cocodataset)))
    print('  - training set size', len(trainset), 'val set size', len(valset))

    trainloader = DataLoader(trainset, batch_size=ag.batch, num_workers=2,
            shuffle=True, collate_fn=cocodataset.getCollateFun())
    valloader = DataLoader(valset, batch_size=ag.batch, num_workers=2,
            collate_fn=cocodataset.getCollateFun())

    print('> Create Model')
    model = JointEmbNet(len(cocodataset.lingual.vocab), dimemb = ag.embdim, dimcnn = ag.cnndim)
    if ag.snapshot:
        print(' * loading parameters from specified snapshot', ag.snapshot)
        state_dict, metainfo, recall, recallT = th.load(ag.snapshot)
        print('   - meta info of the snapshot', metainfo)
        print('   - recall(x-v)', recall)
        print('   - recall(v-x)', recallT)
        model.load_state_dict(state_dict)
    model = model.cuda() if ag.cuda else model.cpu()
    print(model)

    print('> Load glove vectors')
    if False:
        import torchtext as text
        glove = text.vocab.GloVe(name='6B', dim=300)
        notfound = 0
        vocabmiss = []
        for w, i in tqdm.tqdm(vocab_w2i.items()):
            if glove.stoi.get(w, False):
                # found, copy glove.vectors[j] to embeddings[i]
                j = glove.stoi.get(w)
                model.encoder.weight.data[i] = glove.vectors[j]
            else:
                #print(w, 'not found')
                notfound += 1
                vocabmiss.append(w)
        #model.encoder.weight.requires_grad = False
        print('> updated rnn.encoder with glove w2v')
        print(notfound, 'words not found')
        print(vocabmiss)
        del glove, notfound, vocabmiss

    print('> loss function and optimizer')
    crit = PairwiseRankingLoss(margin=0.2, max_violation=True)
    optim = getattr(th.optim, ag.optim)(model.parameters(), lr=ag.lr, weight_decay=1e-7)

    print('>> START TRAINING')
    tbx.add_text('meta/command-line', ' '.join(sys.argv), 0)
    tbx.add_text('meta/git-commit', systemShell(['git', 'log', '-1']), 0)
    tbx.add_text('meta/git-diff', systemShell(['git', 'diff']), 0)
    for epoch in range(ag.maxepoch):

        print('> TRAIN @ Epoch', epoch)
        for iteration, (xs, toks, iids, sids) in enumerate(trainloader, 1):

            # -- go through validation set
            giter = epoch*len(trainloader)+iteration-1
            if giter%ag.testevery == 0:
                evaluation(valset, model,
                           os.path.join(ag.logdir, f'snapshot_latest.pth'),
                           tbx=tbx, giter=giter)
                if ag.snapshot:
                    exit()

            model.train()
            lr = adjustLearningRate(optim, ag.lr, epoch, giter)

            # [forward]
            xs, vs = model(xs, toks, iids, sids)
            loss = crit(xs, vs)

            # [backward]
            optim.zero_grad()
            loss.backward()
            th.nn.utils.clip_grad_norm_(model.parameters(), 2.)
            optim.step()

            # [periodic report]
            if giter % 100 == 0:
                print(f'\033[32;1mEph[{epoch:d}][{iteration:d}/{len(trainloader):d}]:',
                      f'loss {loss.item():.2f}',
                      f'lr {lr}',
                      end='\033[m\n')
                scores = xs.mm(vs.t()).detach().cpu().numpy()
                #print(' -- cnnfeat stat', featstat(xs))
                #print(' -- rnnfeat stat', featstat(hidk))
                recalls, _, ptraw = getRecall(scores)
                print(' * Recall(x->v):', recalls)
                recallsT, _, ptTraw = getRecall(scores.T)
                print(' * Recall(v->x):', recallsT)

                tbx.add_scalar('train/epoch', epoch, giter)
                tbx.add_scalar('train/iteration', iteration, giter)
                tbx.add_scalar('train/lr', lr, giter)
                tbx.add_scalar('train/loss', loss.item(), giter)

    print('> finishing up training process')
    evaluation(valset, model,
               os.path.join(ag.logdir, f'snapshot_latest.pth'),
               tbx=tbx, giter=giter)


def main_rs(ag):
    import IPython
    print('* Initializing Bi-Directional Ranking Shell ...')
    starttime = time.time()

    print('  - Loading Original Annotations ...')
    js = json.load(ag.anno)
    js2 = json.load(ag.annoval)
    js['images'].extend(js2['images'])
    js['annotations'].extend(js2['annotations'])
    print('    we have got', len(js['images']), 'candidate images')
    print('    we have got', len(js['annotations']), 'candidate annotations')
    del js2

    print('  - Loading Dataset ...')
    cocodataset = CocoPreproDataset(ag.cnnpkl, ag.tokspkl)
    print('    dataset size', len(cocodataset))

    print('  - Creating Model')
    model = JointEmbNet(len(cocodataset.lingual.vocab), dimemb = ag.embdim, dimcnn = ag.cnndim)
    print(model)
    print('  - loading parameters from specified snapshot', ag.snapshot)
    state_dict, metainfo, recall, recallT = th.load(ag.snapshot)
    print('    - meta info of the snapshot', metainfo)
    print('    - recall(x-v)', recall)
    print('    - recall(v-x)', recallT)
    model.load_state_dict(state_dict)
    model = model.cuda() if ag.cuda else model.cpu()
    model.eval()
    print('* Initializing Bi-Directional Ranking Shell ... OK')

    print('* Pre-Calculating representations ...')
    cnnfeats, rnnfeats, iids, sids = [], [], [], []
    for giter in tqdm.tqdm(range(len(cocodataset))):
        # FIXME: use dataloader to accellerate calculation
        xs, vs, iid, sid = cocodataset[giter]
        xs = xs.unsqueeze(0)
        xs, vs = model(xs, [vs], [iid], [sid])
        cnnfeats.append(xs.detach().cpu())
        rnnfeats.append(vs.detach().cpu())
        iids.append(iid)
        sids.append(sid)
        #if giter > 100: break
    cnnfeats, rnnfeats = th.cat(cnnfeats), th.stack(rnnfeats)  # 2.5GB, 2.5GB
    print('  - Candidate Set Shape', cnnfeats.shape, rnnfeats.shape)
    print('* Pre-Calculating representations ... OK')

    print('* Launch the Shell ...')
    print('  - Preparation time', time.time() - starttime)
    while True:
        try:
            print('''Image-Text Ranking Shell: ACTION ARGUMENT
            ACTIONS:
              quit        -- quit this shell
              ip          -- temporarily enter ipython
              image <sentence>      -- translate the given sentence to image
              caption <image_path>  -- translate the given image to sentence
            ''')
            cmd = input('\x1b[1;31m><<>\x1b[;m ')
            cmd = shlex.split(cmd)
            if 'quit' in cmd:
                break
            elif 'ip' in cmd[0]:
                IPython.embed()  # Startup an interactive shell here
            elif 'ima' in cmd[0]:
                '''
                translate the given caption to image
                '''
                caption = cmd[1:]
                icaption = [cocodataset.lingual.vocab.vocab.get(x, 0) for x in caption]
                xs, vs, iid, sid = cocodataset[0]
                xs = xs.unsqueeze(0)
                _, reprcap = model(xs, [icaption], [iid], [sid])
                reprcap = reprcap.cpu()
                scores = th.mm(cnnfeats, reprcap.view(-1, 1)).detach().cpu().numpy()
                ranks = np.argsort(scores.flatten()).flatten()[::-1]
                bestmatch = ranks[0]
                bestiid, bestsid = iids[bestmatch], sids[bestmatch]
                print('~ Similar Caption:', bestsid)
                pprint([x for x in js['annotations'] if int(x['id']) == int(bestsid)])
                print('~ Similar Image:', bestiid)
                pprint([x for x in js['images'] if int(x['id']) == int(bestiid)])
                bestimage = os.path.join(os.path.expanduser(ag.pool),
                    [x for x in js['images'] if int(x['id']) == int(bestiid)][0]['file_name'])
                os.system(f'catimg {bestimage}')
            elif 'cap' in cmd[0]:
                '''
                translate the given image to caption
                '''
                # FIXME
                raise NotImplementedError
            else:
                raise ValueError(f'Cannot parse command [{cmd}]')
        except EOFError as e:
            print('quit.')
            break
        except Exception as e:
            print(e)


def main_visrepr(ag):
    print('> visualizing representations')
    cnnfeat, rnnfeat = th.load(ag.pkl)
    scores = th.mm(cnnfeat, rnnfeat.t())
    cnnfeat, rnnfeat = cnnfeat.numpy(), rnnfeat.numpy()

    lab.pcolormesh(cnnfeat, cmap='cool')
    lab.colorbar()
    lab.title('Image Representation by CNN')
    lab.show()

    lab.pcolormesh(rnnfeat, cmap='cool')
    lab.colorbar()
    lab.title('Caption Representation by RNN')
    lab.show()

    lab.pcolormesh(scores.numpy(), cmap='cool')
    lab.colorbar()
    lab.title('Cartesian Product / Cosine Scores')
    lab.show()


if __name__ == '__main__':

    ag = argparse.ArgumentParser()
    ag.set_defaults(func=ag.print_help)
    subag = ag.add_subparsers()
    # [train subparser]
    ag_train = subag.add_parser('train')
    ag_train.set_defaults(func=main_train)
    ag_train.add_argument('--cnnpkl', type=argparse.FileType('rb'),
            default='./coco.all.vgg19')
    ag_train.add_argument('--cnndim', type=int, default=4096)
    ag_train.add_argument('--tokspkl', type=argparse.FileType('rb'),
            default='./coco.all.toks')
    ag_train.add_argument('--embdim', type=int, default=1024)
    ag_train.add_argument('--lr', type=float, default=2e-4)
    ag_train.add_argument('--batch', type=int, default=128)
    ag_train.add_argument('--optim', type=str, default='Adam')
    ag_train.add_argument('--maxepoch', type=int, default=30)
    ag_train.add_argument('--testevery', type=int, default=512)
    ag_train.add_argument('--rnn', type=str, default='GRU')
    ag_train.add_argument('--cuda', default=False, action='store_true')
    ag_train.add_argument('--logdir', default='runs/XXX')
    ag_train.add_argument('--snapshot', default=False)
    # [visrepr subparser]
    ag_visrepr = subag.add_parser('visrepr')
    ag_visrepr.set_defaults(func=main_visrepr)
    ag_visrepr.add_argument('--pkl', type=str)
    # [ranking shell, bi-directional retrieval]
    ag_rs = subag.add_parser('rankshell')
    ag_rs.set_defaults(func=main_rs)
    ag_rs.add_argument('--cnnpkl', type=str, default='./coco.all.vgg19')
    ag_rs.add_argument('--tokspkl', type=str, default='./coco.all.toks')
    ag_rs.add_argument('--snapshot', type=str)
    ag_rs.add_argument('--cuda', action='store_true', default=False)
    ag_rs.add_argument('--pool', type=str, default='../coco/pool')
    ag_rs.add_argument('--embdim', type=int, default=1024)
    ag_rs.add_argument('--cnndim', type=int, default=4096)
    ag_rs.add_argument('--anno', type=argparse.FileType('r'),
            default='../coco/annotations/captions_train2014.json')
    ag_rs.add_argument('--annoval', type=argparse.FileType('r'),
            default='../coco/annotations/captions_val2014.json')
    # parse and dump
    ag = ag.parse_args()
    print('> Dump configuration')
    pprint(vars(ag))

    ag.func(ag)

'''
>>> python3 th-caprank-rnn.py train --cuda --logdir runs/vgg19-gru
>>> python3 th-caprank-rnn.py train --cuda --snapshot testrun/model_best.pth
>>> python3 th-caprank-rnn.py visrepr --pkl feat_best.pth
>>> python3 th-caprank-rnn.py rankshell --cuda --snapshot runs/vgg19-gru-vocab/model_best.pth --pool ~/cocopool

GRU is sometimes better than LSTM.
A 512-dimensional embedding space may be difficult for the SGD optimizer, compared to a 1024-dimensional embedding space.
For CenterCrop setting, VGG19 vector representations works better than that from ResNet152, both without fine-tune.
Initialization matters. However it's hard to explain what initilization method works better and why.
BatchNorm before L2-normalization makes some differences, but not all of them are good.
Canceling the Image Representation normalization (Kiros' approach) makes some differences, but it is theoretically problematic.
Sentences need to be padded with '<start>' at the head and '<end>' at the tail.
Hard negative helps improve the performance by a large margin.
Image representations from TenCrop is way better than that from CenterCrop.
We should not keep the words with very low frequency.

-> 613.4/800, Best of Plain
   Needs Refresh

-> 622.2/800, Best of Hard-Negative
 * Recall(x->v): r@mean   8.1,  r@med   2.0,  r@1  40.7,  r@5  80.8,  r@10  91.2,  r@50  97.8
 * Recall(v->x): r@mean   8.3,  r@med   2.0,  r@1  42.3,  r@5  81.0,  r@10  90.6,  r@50  97.8
'''
