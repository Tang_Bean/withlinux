'''
Image Feature Extractor Using Convolutional Neural Network.
Copyright (C) 2018 Lumin

References:
http://pytorch.org/docs/master/torchvision/transforms.html
http://pytorch.org/docs/master/torchvision/models.html
https://github.com/pytorch/vision/pull/310
'''
import os
import sys
import argparse
from pprint import pprint
import random
from PIL import Image
import json
import pickle
import pylab
import random

import torch as th
from torch.utils.data import Dataset, DataLoader
import torchvision as vision
import numpy as np

sys.path.append('../')
from FlashLight import *


class CocoImRawDataset(Dataset):
    '''
    for loading raw coco dataset
    '''
    def __init__(self, anno, pool, transform = None):
        if isinstance(anno, str):
            annotation = json.load(open(anno, 'r'))
        else:
            annotation = json.load(anno)
        self.pool = pool
        self.images = annotation['images']
        self.transform = transform
    def __len__(self):
        return len(self.images)
    def __getitem__(self, index):
        imageid = self.images[index]['id']
        filename = self.images[index]['file_name']
        fpath = os.path.join(self.pool, filename)
        image = Image.open(fpath).convert('RGB')
        if self.transform is not None:
            image = self.transform(image)
        return image, imageid
    def getCollateFun(self):
        def _collate(data):
            images, imageids = zip(*data)
            images = th.stack([x.squeeze() for x in images], 0)
            if len(images.shape) == 5:
                images = images.squeeze()
            return images, imageids
        return _collate


def getTransform(crop):
    '''
    get the specified transform.
    Note, Resize(256) is different with Resize((256, 256)), where the former
    keeps horizontal-vertical ratio while the latter does not.
    '''
    trans = []
    normalizer = vision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                              std=[0.229, 0.224, 0.225])
    totensor   = vision.transforms.ToTensor()
    if crop in ('CenterCrop',):
        #trans.append(vision.transforms.Resize((256, 256))
        trans.append(vision.transforms.Resize(256))
        trans.append(getattr(vision.transforms, crop)(224))
        trans.extend([totensor, normalizer])
    elif crop in ('TenCrop',):
        #trans.append(vision.transforms.Resize((256, 256))
        trans.append(vision.transforms.Resize(256))
        trans.append(vision.transforms.TenCrop(224))
        trans.append(vision.transforms.Lambda(
            lambda crops: [totensor(crop) for crop in crops]))
        trans.append(vision.transforms.Lambda(
            lambda crops: [normalizer(crop) for crop in crops]))
        trans.append(vision.transforms.Lambda(
            lambda crops: th.stack(crops) ))
    else:
        raise NotImplementedError
    return vision.transforms.Compose(trans)


class Extractor(th.nn.Module):
    '''
    Any-Module feature extractor.

    Note that, if you are extracting the laster layer, this way is simpler:
    >>> cnn2 = getattr(vision.models, ag.cnn)(pretrained=True) # pre-trained
    >>> cnn2.fc = th.nn.Sequential()
    >>> cnn2.eval()
    '''
    def __init__(self, model, featname, featpost, use_cuda=False):
        super(Extractor, self).__init__()
        self.model = model.cuda() if use_cuda else model.cpu()
        self.featname = featname
        self.featpost = featpost
        self.outputs = {}

        def _hook(module, input, output):
            x = output.clone().detach()
            x = self.doPost(self.featpost, x)
            self.outputs[featname] = x

        spec = featname.strip().split('.')
        spec = ''.join(['self.model', *(f'._modules[{repr(xname)}]' for xname in spec)])
        print(' * feature extraction layer', spec)
        self.handle = eval(spec).register_forward_hook(_hook)

    def forward(self, *args):
        output = self.model(*args)
        return self.outputs[self.featname]

    def doPost(self, postspec, x):
        _ident = lambda x: x
        _mean0 = lambda x: x.mean(0)
        _squeeze = lambda x: x.squeeze()
        _unsqueeze0 = lambda x: x.unsqueeze(0)
        _norm = lambda x: th.nn.functional.normalize(x)
        _norm0 = lambda x: th.nn.functional.normalize(x, dim=0)
        spec = postspec.strip().split('+')
        for sp in spec:
            x = eval(f'_{sp}')(x)
        return x


def featstat(feat):
    return ' '.join([f'mean {feat.mean():.5f}',
        f'var {feat.var():.5f}', f'min {feat.min():.5f}',
        f'max {feat.max():.5f}'  ])


class CocoVRepDataset(Dataset):
    '''
    For loading pre-processed dataset
    '''
    def __init__(self, cnnfeats: str = './visual.py.cnnfeats.pkl'):
        self.cnnfeats = pklLoad(cnnfeats)
        self.imageids = list(sorted(self.cnnfeats.keys()))
    def __len__(self):
        return len(self.cnnfeats)
    def __getitem__(self, index):
        imageid = self.imageids[index]
        cnnfeat = self.cnnfeats[imageid]
        return cnnfeat, imageid
    def byiid(self, index):
        return self.cnnfeats[index]


if __name__ == '__main__':

    if len(sys.argv)>1 and sys.argv[1] == 'check':

        print('>> SPECIAL MODE: FEATURE CHECK')
        print('  ', f'check file {sys.argv[2]} for', f'{sys.argv[3]} samples')
        from collections import Counter
        from pprint import pprint
        cnnfeat = pickle.load(open(sys.argv[2], 'rb'))
        samples = np.concatenate([cnnfeat[k] for k in
            random.choices(list(cnnfeat.keys()), k=int(sys.argv[3]))])
        #samples = np.concatenate([v for k, v in cnnfeat.items()])
        samples = np.clip(samples, a_min=0, a_max=None)
        ctr = Counter()
        ctr.update(np.argmax(samples, axis=1))
        pprint(ctr)
        print(featstat(samples))

        print('var axis 0', np.var(samples, axis=0).mean())
        print('var axis 1', np.var(samples, axis=1).mean())

        pylab.pcolormesh(samples, cmap='cool')
        pylab.colorbar()
        pylab.show()
        raise SystemExit

    elif sys.argv[1] == 'test':

        print('> TEST')
        data = CocoVRepDataset('./coco.all.res18')
        print(len(data), data[0])
        for cnnfeat, imageid in data:
            pass
        print('> test ok')
        exit()

    elif sys.argv[1] == 'merge':

        print('> MERGE:', sys.argv[2], sys.argv[3], '->', sys.argv[4])
        cnnfeat = pklLoad(sys.argv[2])
        cnnfeat.update(pklLoad(sys.argv[3]))
        pklSave(cnnfeat, sys.argv[4])


    ag = argparse.ArgumentParser()
    ag.add_argument('--anno', type=argparse.FileType('r'),
            default='../coco/annotations/captions_train2014.json')
    ag.add_argument('--pool', type=str, default='../coco/pool')
    ag.add_argument('--crop', type=str, default='CenterCrop')
    ag.add_argument('--pkl', type=str, default=f'{__file__}.imrepr.pkl')
    ag.add_argument('--cuda', default=False, action='store_true')
    ag.add_argument('--cnn', type=str, default='resnet18')
    ag.add_argument('--featname', type=str, default='avgpool/ident')
    ag.add_argument('--partial', default=False)
    ag = ag.parse_args()
    pprint(vars(ag))

    print('> load extractor/cnn')
    featname, featpost = ag.featname.split('/')
    cnn = getattr(vision.models, ag.cnn)(pretrained=True) # pre-trained
    extractor = Extractor(cnn, featname, featpost, ag.cuda)
    extractor.eval()
    print(extractor)

    print('> setup dataloader')
    cocoraw = CocoImRawDataset(ag.anno, ag.pool, getTransform(ag.crop))
    loader = DataLoader(dataset = cocoraw, batch_size = 1, shuffle = False,
                        pin_memory = True, num_workers = 2,
                        collate_fn = cocoraw.getCollateFun())

    print(f'> The result will be saved to {ag.pkl} when complete')
    features = {}
    for iteration, (image, (imageid,)) in enumerate(loader):
        image = image.cuda() if ag.cuda else image
        feature = extractor(image)
        features[imageid] = feature.detach().cpu()
        if iteration % (len(loader)//100) == 0:
            print()
            print(' * [0/2] image shape', image.shape, imageid)
            print(' * [1/2] feature shape', feature.shape)
            print(' * [2/2] feature stat', featstat(feature))
        print('\0337\033[K\033[32;1m> image', iteration, '/', len(loader),
                f' \timageid({imageid})',
                f' \t{iteration*100/len(loader):.1f}%%',
                end='\033[m\0338')
        sys.stdout.flush()
        if ag.partial and iteration > int(ag.partial):
            break

    print(f'> saving to {ag.pkl}')
    with open(ag.pkl, 'wb') as f:
        pickle.dump(features, f, pickle.HIGHEST_PROTOCOL)

    '''
    >>> python3 visual.py --cuda --pool ~/cocopool --crop CenterCrop --cnn resnet18 --featname avgpool/squeeze+norm0
    >>> python3 visual.py --cuda --pool ~/cocopool --crop TenCrop --cnn resnet18 --featname avgpool/squeeze+mean0
    >>> python3 visual.py --cuda --pool ~/cocopool --crop CenterCrop --cnn vgg19 --featname classifier.4/squeeze+norm0
    >>> python3 visual.py --cuda --pool ~/cocopool --crop TenCrop --cnn vgg19 --featname classifier.4/squeeze+mean0+norm0

    >>> python3 visual.py --cuda --pool ~/cocopool --crop TenCrop --cnn vgg19 --featname classifier.4/squeeze+mean0+norm0 --pkl coco.train.vgg19p --anno ../coco/annotations/captions_val2014.json
    >>> python3 visual.py --cuda --pool ~/cocopool --crop TenCrop --cnn vgg19 --featname classifier.4/squeeze+mean0+norm0 --pkl coco.val.vgg19p --anno ../coco/annotations/captions_val2014.json

    >>> python3 visual.py merge coco.train.vgg19 coco.val.vgg19 coco.all.vgg19
    >>> python3 visual.py check visual.py.cnnfeats.pkl 1000
    TODO: should we keep the ratio?
    '''
