'''
Lingual Objects, including Vocabulary, ...
Copyright (C) 2018 Lumin
'''
import sys, os, re, _io
import contextlib
import argparse
import pickle
from collections import Counter, defaultdict
import ujson as json  # Fastest Json
import string
from pprint import pprint
from typing import *
import gzip, zstd
import unittest

import nltk
from torch.utils.data import Dataset
sys.path.append('../')
from FlashLight import *


def cocoTokenize(anno: _io.TextIOWrapper) -> (dict, Counter):
    '''
    tokenize all the annotations and return a dictionary containing
    the results, indexed by the key specified by keyname.
    '''
    anno.seek(0)
    j_orig = json.load(anno)
    tokens = dict()
    wctr = Counter()
    for i, annotation in enumerate(j_orig['annotations']):
        tok = tokenize(annotation['caption'])
        wctr.update(tok)
        tokens[int(annotation['id'])] = (tok, int(annotation['image_id']))
        print('\0337\033[K>', i, '/', len(j_orig['annotations']),
                '-> {:.1f}%'.format(100*i/len(j_orig['annotations'])),
                end='\0338')
        sys.stdout.flush()

    return tokens, wctr


def filterTokens(tokens: Dict, vocab: List[str]) -> Dict:
    '''
    filter the given tokens dictionary, dropping the words which doesn't
    appear in the given dictionary.
    '''
    newtokens = {}
    vocValid = {k: True for k in vocab}  # Dict can speed up token validation
    filter = lambda x: x if vocValid.get(x, False) else '<unknown>'
    for xid, (sent, iid) in tokens.items():
        newtokens[int(xid)] = (list(map(filter, sent)), int(iid))
    return newtokens


class CocoLtokDataset(Dataset):
    '''
    load tokens
    '''
    def __init__(self, tokpath: str = './coco.all.toks', threshold: int = 5):
        tokens, wctr = jsonLoad(tokpath)
        self.vocab = Vocabulary(wctr, threshold)
        tokens = filterTokens(tokens, self.vocab.vocablist)

        self.sentids = list(sorted(sid for sid in tokens.keys()))
        self.imagids = list(sorted(set(iid for (toks, iid) in tokens.values())))

        self._bysid = {}
        self._byiid = defaultdict(list)
        for sid, (toks, iid) in tokens.items():
            self._bysid[sid] = (toks, iid)
            self._byiid[iid].append((toks, sid))
    def __len__(self):
        return len(self.sentids)
    def __getitem__(self, index):  # by internal ID
        sid = self.sentids[index]
        tok, iid = self._bysid[sid]
        tok = ['<start>'] + tok + ['<end>']
        return self.vocab(tok), iid, sid
    def byiid(self, index):
        toks, sids = zip(*self._byiid[index])
        toks = [['<start>'] + x + ['<end>'] for x in toks]
        return [(self.vocab(tok), sid) for (tok, sid) in zip(toks, sids)]
    def bysid(self, index):
        tok, iid = self._bysid[index]
        tok = ['<start>'] + tok + ['<end>']
        return self.vocab(tok), iid


if __name__ == '__main__':

    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        print('<> TEST MODE')
        print('> load dataset')
        data = CocoLtokDataset(sys.argv[2])
        print('> vocabulary size')
        print(len(data.vocab))
        print(data[0])
        for tok, iid, sid in data:
            pass
        print(data.byiid(452441))
        print(data.bysid(100))
        print('> test ok')
        exit()

    ag = argparse.ArgumentParser()
    ag.add_argument('--srcanno', type=argparse.FileType('r'),
            default='../coco/annotations/captions_train2014.json')
    ag.add_argument('--srcannoval', type=argparse.FileType('r'),
            default='../coco/annotations/captions_val2014.json')
    ag.add_argument('--saveto', type=str, default=f'{__file__}.all.toks')
    ag = ag.parse_args()
    pprint(vars(ag))

    alltokens, trainvocab = cocoTokenize(ag.srcanno)
    valtokens, valvocab   = cocoTokenize(ag.srcannoval)
    alltokens.update(valtokens)

    jsonSave([alltokens, trainvocab], ag.saveto)

'''
>>> python3 lingual.py --saveto coco.all.toks
>>> python3 lingual.py test coco.all.toks
>>> python3 -m unittest -v lingual.py
'''
