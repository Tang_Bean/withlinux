-- scene graph -> tree
-- sg2tr.lua

sg2tr = {}

function sg2tr.loadSGs(sgpath)
	assert(sgpath)
	local sgf = io.open(sgpath, 'r')
	local sgf_content = sgf:read()
	local cjson = require 'cjson'
	local SGs = cjson.decode(sgf_content)
	return SGs
end

function sg2tr.getListNodes(sg)
	local listNodes = {}
	for k,v in pairs(sg.nodes) do
		table.insert(listNodes, v.name)
	end
	return listNodes
end

function sg2tr.shatterStringTable(st)
	assert(type(st) == "table")
	local wordlistraw = {}
	function sanitize_(word)
		local w1,_ = string.gsub(word, ',', '')
		local w2,_ = string.gsub(w1, '%.', '')
		return w2
	end
	for k, v in pairs(st) do
		table.foreachi(stringx.split(v, ' '), function(k_,v_) table.insert(wordlistraw, sanitize_(v_)) end)
	end
	return wordlistraw
end

function sg2tr.collectWordsSaliency(wl)
	local saliencyDict = {}
	for k, v in pairs(wl) do
		if saliencyDict[tostring(v)] == nil then
			saliencyDict[tostring(v)] = 1
		else
			saliencyDict[tostring(v)] = 1 + saliencyDict[tostring(v)]
		end
	end
	--for k, v in pairs(saliencyDict) do
	--	saliencyDict[k] = v / #wl
	--end
	return saliencyDict
end

function sg2tr.SDLookup(sd, q)
	return sd[q]
end

function sg2tr.getSaliency(listsNodes, refss)
	assert(#listsNodes == #refss)
	local SLs = {} -- saliency tables
	for i = 1, #listsNodes do
		local cur_listNodes = listsNodes[i] -- a table of node names
		print(cur_listNodes)
		local cur_refs = refss[i] -- a table of strings
		print(cur_refs)

		local cur_wordlistraw = sg2tr.shatterStringTable(cur_refs)
		print(' -> DEBUG 1')
		print(cur_wordlistraw)
		local cur_saliencyDict = sg2tr.collectWordsSaliency(cur_wordlistraw)
		print(' -> DEBUG 2')
		print(cur_saliencyDict)
		print(sg2tr.SDLookup(cur_saliencyDict, "is"))

		local cur_saliency = {}
		for k, v in pairs(cur_listNodes) do
			print(' -> lookup saliency', v, sg2tr.SDLookup(cur_saliencyDict, v))
			local sl = sg2tr.SDLookup(cur_saliencyDict, v)
			if sl ~= nil then
				table.insert(cur_saliency, sl)
			else
				--\ handling special saliency
				local special_saliency = 0
				local special_saliency_token = ""

				--\ helper function
				local function saliency_dict_most_match(d85, v85)
					local sps86 = 0
					local sps86_token = ""
					for k86, v86 in pairs(d85) do
						local found89, _ = string.find(k86, v85)
						if found89 ~= nil then
							sps86 = v86
							sps86_token = k86
						end
					end
					return sps86, sps86_token
				end
				function maxsl(d2)
					-- @input d2 saliency dict, word -> number (>=0) mapping
					-- 	-- @input q2 query, string
					local mkey5 = ""
					local mval5 = 0
					for k5, v5 in pairs(d2) do
						if v5 > mval5 then
							mkey5 = k5
							mval5 = v5
						end
					end
					return mkey5, mval5
				end

				local slashfound, _ = string.find(v, '/')
				if slashfound ~= nil then
					--\\ case 1: contains slash (corenlp SG synonyms)
					local synonyms = stringx.split(v, '/')
					local syn15_sls = {}
					for _, syn02 in pairs(synonyms) do
						local m17, tk17 = saliency_dict_most_match(cur_saliencyDict, syn02)
						syn15_sls[syn02] = m17
					end
					print('-> special case, synonyms found')
					print(syn15_sls)
					local m122, k122 = maxsl(syn15_sls)
					print('  -> select', k122, m122)
					special_saliency = k122
					special_saliency_token = m122
				else
					--\\ case 2: countable nouns
					special_saliency, special_saliency_token = saliency_dict_most_match(cur_saliencyDict, v)
					--for kk, vv in pairs(cur_saliencyDict) do
					--	local found,_ = string.find(kk, v)
					--	if found ~= nil then
					--		special_saliency = vv
					--		special_saliency_token = kk
					--	end
					--end
				end
				print(" --> special saliency: ", v, "similar to", special_saliency_token, special_saliency)
				table.insert(cur_saliency, special_saliency)
			end
		end
		print(cur_saliency)
		table.insert(SLs, cur_saliency)
	end
	-- normalize SLs : foreach item -> item / sum_item
	for i = 1, #SLs do
		-- get local sum
		local localsum = 0
		for k, v in pairs(SLs[i]) do
			localsum = localsum + v
		end
		print("  -> localsum", localsum)
		-- do normalize
		for k, v in pairs(SLs[i]) do
			SLs[i][k] = v / localsum
		end
	end
	return SLs -- same structure as listsNodes
end

function sg2tr.createMapNodeSaliency(listsNodes, listsSLs)
	assert(listsNodes); assert(listsSLs);
	assert(#listsNodes == #listsSLs);
	local listsn2s = {}
	for i = 1, #listsNodes do
		assert(#listsNodes[i] == #listsSLs[i])
		local n2s = {}
		for j = 1, #listsNodes[i] do
			table.insert(n2s, {listsNodes[i][j], listsSLs[i][j]})
		end
		table.insert(listsn2s, n2s)
	end
	return listsn2s
end

function sg2tr.selectNodesBySaliency(listsNodes, listsSL)
	local listsCandidates = {} -- a list of lists

	assert(#listsNodes == #listsSL)
	for i = 1, #listsNodes do
		assert(#listsNodes[i] == #listsSL[i])

		local candidates = {}
		-- select nodes for SG[i]
		require 'torch'
		local val, idx = torch.sort(torch.Tensor(listsSL[i]), true) -- descending
		table.insert(candidates, listsNodes[i][idx[1]])
		table.insert(candidates, listsNodes[i][idx[2]])

		table.insert(listsCandidates, candidates)
	end
	--print(listsCandidates)
	return listsCandidates -- similar structure as listsNodes
end

function sg2tr.selectOneNodeBySaliency(listsNodes, listsSL)
	local listsCandidates = {} -- a list of lists

	assert(#listsNodes == #listsSL)
	for i = 1, #listsNodes do
		assert(#listsNodes[i] == #listsSL[i])

		local candidate = ''
		-- select nodes for SG[i]
		require 'torch'
		local val, idx = torch.sort(torch.Tensor(listsSL[i]), true) -- descending
		candidate = listsNodes[i][idx[1]]

		table.insert(listsCandidates, candidate)
	end
	--print(listsCandidates)
	return listsCandidates -- similar structure as listsNodes
end

function sg2tr._isSource(node, relation)
	assert(node); assert(relation)
	return relation.source == node
end

function sg2tr._isTarget(node, relation)
	assert(node); assert(relation)
	return relation.target == node
end

function sg2tr.getNodeDeps(sg, node)
	assert(sg); assert(node)
	-- @input a sg and a node
	-- @output a list of relations where the sources are the specified node
	local relations = sg.relations
	local deps = {}
	for k, v in pairs(relations) do
		if sg2tr._isSource(node, v) then table.insert(deps, v) end
	end
	return deps
end

function sg2tr.getNodeRDeps(sg, node)
	assert(sg); assert(node)
	-- @input a sg and a node
	-- @output a list of relations where the targets are the specified node
	local relations = sg.relations
	local rdeps = {}
	for k, v in pairs(relations) do
		if sg2tr._isTarget(node, v) then table.insert(rdeps, v) end
	end
	return rdeps
end

function sg2tr.getListsNodeDeps(sgs, nodes)
	assert(sgs); assert(nodes);
	assert(#sgs == #nodes);
	-- @input a list of sgs and a list of nodes
	-- @output a list of (list of relations where ...)
	local listsdeps = {}
	for i = 1, #sgs do
		local deps = sg2tr.getNodeDeps(sgs[i], nodes[i])
		table.insert(listsdeps, deps)
	end
	return listsdeps
end

function sg2tr.getListsNodeRDeps(sgs, nodes)
	assert(sgs); assert(nodes);
	assert(#sgs == #nodes);
	local listsrdeps = {}
	for i = 1, #sgs do
		local rdeps = sg2tr.getNodeRDeps(sgs[i], nodes[i])
		table.insert(listsrdeps, rdeps)
	end
	return listsrdeps
end

function sg2tr.getChainByOneNode(sg, node, n2s)
	assert(sg); assert(node);
	-- @input an sg and a node
	-- @output a special list (chain) of relations
	local dbg = true
	local error_thre = 10
	local chain = {}

	local function _n2slookup(n2s_, name_)
		local res = nil
		for k, v in pairs(n2s_) do
			if v[1] == name__ then res = v[2]; break end
		end
		return res or 0 -- res or 0 to eliminate error
	end
	local function _in_chain(node, chain)
		for k, v in pairs(chain) do
			if v.source == node or v.target == node then return true end
		end
		return false
	end
	--local function _deps_filter(deps_, filternode_)
	--	print(' XD | deps_ |', filternode_, deps_)
	--	local newdeps = {}
	--	for k, v in pairs(deps_) do
	--		if not _in_chain(filternode_, deps_) then table.insert(newdeps, v) end
	--	end
	--	print(' XD | new deps_ |', filternode_, newdeps)
	--	return newdeps
	--end
	local function _deps_filter_xsource(deps_, chain_)
		-- source node of dep must not be included in chain
		print(' XD | deps_ |', deps_)
		local newdeps = {}
		for k, v in pairs(deps_) do
			if not _in_chain(v.source, chain_) then table.insert(newdeps, v) end
		end
		print(' XD | new deps_ |', newdeps)
		return newdeps
	end
	local function _deps_filter_xtarget(deps_, chain_)
		-- source node of dep must not be included in chain
		print(' XD | deps_ |', deps_)
		local newdeps = {}
		for k, v in pairs(deps_) do
			if not _in_chain(v.target, chain_) then table.insert(newdeps, v) end
		end
		print(' XD | new deps_ |', newdeps)
		return newdeps
	end
	
	local function _idx_bestdep(deps_, chain_)
		local scores = {}
		for k, v in pairs(deps_) do
			if _n2slookup(n2s, v.target) == nil then
				print("* v.target", v.target)
				print("* n2s", n2s)
				error("n2s lookup error")
			end
			table.insert(scores, _n2slookup(n2s, v.target))
		end
		assert(#scores == #deps_)
		local _, idx = torch.sort(torch.Tensor(scores), true) -- x1 >= x2
		if not _in_chain(cur_node_, chain_) then
			return idx[1]
		else
			for i = 1, #idx do
				if i ~= 1 then
					if not _in_chain(deps_[idx[i]].target) then return idx[i] end
				end
			end
		end
		print(' | note: avoid loop')
		return nil
	end
	local function _idx_bestrdep(deps_, chain_)
		local scores = {}
		for k, v in pairs(deps_) do
			--if n2s[v.source] == nil then
			if _n2slookup(n2s, v.source) == nil then
				print("* v.source", v.source)
				print("* n2s", n2s)
				error("n2s lookup error")
			end
			table.insert(scores, _n2slookup(n2s, v.source))
		end
		assert(#scores == #deps_)
		local _, idx = torch.sort(torch.Tensor(scores), true) -- x1 >= x2
		if not _in_chain(cur_node_, chain_) then
			return idx[1]
		else
			for i = 1, #idx do
				if i ~= 1 then
					if not _in_chain(deps_[idx[i]].source) then return idx[i] end
				end
			end
		end
		print(' | note: avoid loop')
		return nil
	end

	-- [[ pass 1 ]] -- expand dep chain
	--
	local cursor_node = node
	local cursor_deps = sg2tr.getNodeDeps(sg, cursor_node)
	--cursor_deps = _deps_filter(cursor_deps, cursor_node)
	cursor_deps = _deps_filter_xtarget(cursor_deps, chain)
	if dbg then print(" | cursor node->", cursor_node); print(" | cursor node_deps->", cursor_deps); end
	if #cursor_deps > 0 then
		while (#cursor_deps > 0) do
			if #cursor_deps == 0 then break end
			-- update chain
			local cursor_ring
			--if #cursor_deps == 1 then
			--	-- only one candidate ring
			--	table.insert(chain, cursor_deps[1])
			--	cursor_ring = cursor_deps[1]
			--	-- next node
			--	cursor_node = cursor_ring.target
			--	cursor_deps = sg2tr.getNodeDeps(sg, cursor_node)
			--	if dbg then print(" | cursor node->", cursor_node); print(" | cursor node_deps->", cursor_deps); end
			--	if #cursor_deps == 0 then break end
			--else
				-- select a good candidate ring
				local best_idx = _idx_bestdep(cursor_deps, chain)
				if best_idx == nil then break end
				table.insert(chain, cursor_deps[best_idx])
				cursor_ring = cursor_deps[best_idx]
				-- next node
				cursor_node = cursor_ring.target
				cursor_deps = sg2tr.getNodeDeps(sg, cursor_node)
				--cursor_deps = _deps_filter(cursor_deps, cursor_node)
				cursor_deps = _deps_filter_xtarget(cursor_deps, chain)
				if dbg then print(" | cursor node->", cursor_node); print(" | cursor node_deps->", cursor_deps); end
				if #cursor_deps == 0 then break end
			--end
		end
	end

	-- [[ pass 2 ]] -- expand rdep chain
	--
	local cursor_node = node
	local cursor_rdeps = sg2tr.getNodeRDeps(sg, cursor_node)
	--cursor_rdeps = _deps_filter(cursor_rdeps, cursor_node)
	cursor_rdeps = _deps_filter_xsource(cursor_rdeps, chain)
	if dbg then print(" | R | cur node->", cursor_node); print(" | cur node_deps->", cursor_rdeps); end
	if #cursor_rdeps > 0 then
		local counterwhile = 0
		while (#cursor_rdeps > 0) do
			if #cursor_rdeps == 0 then break end
			--if #cursor_rdeps == 1 then
			--	table.insert(chain, 1, cursor_rdeps[1])
			--	local cursor_ring = cursor_rdeps[1]
			--	-- next node
			--	cursor_node = cursor_ring.source
			--	cursor_rdeps = sg2tr.getNodeRDeps(sg, cursor_node)
			--	if dbg then print(" | R | cur node->", cursor_node); print(" | cur node_deps->", cursor_rdeps); end
			--	if #cursor_rdeps == 0 then break end
			--else
				-- select from candidate rdeps
				local best_idx = _idx_bestrdep(cursor_rdeps, chain)
				if best_idx == nil then break end
				local cursor_ring = cursor_rdeps[best_idx]
				table.insert(chain, 1, cursor_ring)
				-- next node
				cursor_node = cursor_ring.source
				cursor_rdeps = sg2tr.getNodeRDeps(sg, cursor_node)
				--cursor_rdeps = _deps_filter(cursor_rdeps, cursor_node)
				cursor_rdeps = _deps_filter_xsource(cursor_rdeps, chain)
				if dbg then print(" | R | cur node->", cursor_node); print(" | cur node_deps->", cursor_rdeps); end
				if #cursor_rdeps == 0 then break end
			--end
			counterwhile = counterwhile + 1
			if counterwhile > error_thre then error("E: while counter exceeds threshold") end
		end
	end
	-- if len(chain) is 0 now, there must be no relationship in the original SG, construct a special chain for it
	if #chain == 0 then
		table.insert(chain,{source=node, relation="", target=""})
	end
	print(" -->DEBUG: node expand to chain of size ", #chain)
	return chain
end

function sg2tr.getListsChainByOneNodes(sgs, nodes, listsn2s)
	assert(sgs); assert(nodes); assert(listsn2s);
	assert(#sgs == #nodes); assert(#sgs == #listsn2s);
	local listChains = {}
	for i = 1, #sgs do
		local chain = sg2tr.getChainByOneNode(sgs[i], nodes[i], listsn2s[i])
		table.insert(listChains, chain)
	end
	return listChains
end

function sg2tr.getAttrs(sg, node)
	-- @output a list of attrs, or {}
	assert(sg); assert(node);
	local nodes = sg.nodes
	for k, v in pairs(nodes) do
		if v.name == node then return v.attr end
	end
	return {}
end

function sg2tr.dumpChain(chain)
	assert(chain);
	for k, v in pairs(chain) do
		print(' [chain dump] | s -> r -> t', v.source, v.relation, v.target)
	end
end

function sg2tr.addAttrChain(sg, chain)
	-- @output a special form of chain
	assert(sg); assert(chain)
	for i = 1, #chain do
		local source = chain[i].source
		local rel = chain[i].rel
		local target = chain[i].target
		
		local source_attr = sg2tr.getAttrs(sg, source)
		local target_attr = sg2tr.getAttrs(sg, target)
		if #source_attr > 0 then
			chain[i].source = {source, source_attr}
		end
		if #target_attr > 0 then
			chain[i].target = {target, target_attr}
		end
	end
	return chain
end

function sg2tr.listAddAttrChain(sgs, chains)
	assert(sgs); assert(chains);
	assert(#sgs == #chains)
	for i = 1, #sgs do
		chains[i] = sg2tr.addAttrChain(sgs[i], chains[i])	
	end
	return chains
end

function sg2tr.grossAttrChain2Sentence(achain)
	assert(achain)
	local seq = {}
	for k, v in pairs(achain) do
		local source = v.source
		local rel = v.relation
		local target = v.target

		if k == 1 then
			-- add source to sequence
			if type(source) == 'string' then
				table.insert(seq, source)
			elseif type(source) == 'table' then
				for kk, vv in pairs(source[2]) do
					table.insert(seq, vv)
				end
				table.insert(seq, source[1])
			else
				print(source)
				error('invalid source type')
			end
		end
		-- add relation to sequence
		table.insert(seq, rel)
		-- add target to sequence
		if type(target) == 'string' then
			table.insert(seq, target)
		elseif type(target) == 'table' then
			for kk, vv in pairs(target[2]) do
				table.insert(seq, vv)
			end
			table.insert(seq, target[1])
		else
			print(target)
			error('invalid target type')
		end
	end
	print(' |-> [achain 2 sent] DEBUG: sequence length', #seq)
	print(seq)
	local sent = stringx.join(' ', seq)
	return sent
end

function sg2tr.listGrossAttrChain2Sentence(listachain)
	assert(listachain)
	local listsents = {}
	for k, v in pairs(listachain) do
		local sent = sg2tr.grossAttrChain2Sentence(v)
		table.insert(listsents, sent)
	end
	return listsents
end

-- @interface batch SG -> batch ST
function sg2tr.sg2tr(listSG, listTexts)
	assert(listSG); assert(listTexts);
	-- get list of nodes
	local listNodes = {}
	for i = 1, #listSG do
		local nodes = sg2tr.getListNodes(listSG[i])
		table.insert(listNodes, nodes)
	end
	-- get saliency
	local listSLs = sg2tr.getSaliency(listNodes, listTexts)
	-- select a node for each graph
	local listOneNode = sg2tr.selectOneNodeBySaliency(listNodes, listSLs)
	-- create n2s mapping
	local listN2S = sg2tr.createMapNodeSaliency(listNodes, listSLs)
	-- get chains
	local listChains = sg2tr.getListsChainByOneNodes(listSG, listOneNode, listN2S)
	--sg2tr.dumpChain(listsChains[1])
	-- convert chain to achain
	local listAChains = sg2tr.listAddAttrChain(listSG, listChains)
	return listAChains
end

function sg2tr.test()
	local SGs = sg2tr.loadSGs('./spice_sg_test_json.json')
	--local SGs = sg2tr.loadSGs('./spice_sg_refs_json.json')
	print(SGs)
	print('found '..tostring(#SGs)..' SGs')

	local listsNodes = {}
	for i = 1, #SGs do
		table.insert(listsNodes, sg2tr.getListNodes(SGs[i]))
	end
	print(listsNodes)

    local tester_test = "two fedex trucks parked on the side of the street . "
	local tester_refs = {
		"a couple of fed ex delivery trucks parked outside of a building.",
		"two fedex trucks parked on a side of a street with tall buidings behind them.",
		"fedex trucks parked on the side of the street while cars wait in traffic.",
		"there are a few delivery vans parked on the side of the road. "
	}
	print(' -> test wordlistraw')
	local wordlistraw = sg2tr.shatterStringTable(tester_refs)
	print(wordlistraw)

	print(' -> test saliencyDict')
	local saliencyDict = sg2tr.collectWordsSaliency(wordlistraw)
	print(saliencyDict)

	print(' -> test saliencys')
	local listsRefs = {tester_refs, tester_refs}
	--local listsRefs = useful_listrefs
	local saliencys = sg2tr.getSaliency(listsNodes, listsRefs)
	print(listsNodes)
	print(saliencys)

	print(' -> test selections')
	local selectedNodes = sg2tr.selectNodesBySaliency(listsNodes, saliencys)
	print(selectedNodes)

	print(' -> test one-shot selections')
	local oneNodes = sg2tr.selectOneNodeBySaliency(listsNodes, saliencys)
	print(oneNodes)

	print(' -> test node deps')
	local oneNodes_deps = sg2tr.getListsNodeDeps(SGs, oneNodes)
	print(oneNodes_deps)

	print(' -> test node rdeps')
	local oneNodes_rdeps = sg2tr.getListsNodeRDeps(SGs, oneNodes)
	print(oneNodes_rdeps)

	print(' -> create n2s maps')
	local listsN2S = sg2tr.createMapNodeSaliency(listsNodes, saliencys)
	print(listsN2S)

	print(' -> test chain expansion by one node')
	--print(SGs, oneNodes, listsN2S)
	local listsChains = sg2tr.getListsChainByOneNodes(SGs, oneNodes, listsN2S)
	print(listsChains)

	print(' -> test dump chain')
	sg2tr.dumpChain(listsChains[1])

	print(' -> test get attr')
	print(sg2tr.getAttrs(SGs[1], 'truck'))

	print(' -> test add attr to chain')
	local listsAChains = sg2tr.listAddAttrChain(SGs, listsChains)
	print(listsAChains)

	print(' -> test achain to sent, GROSS')
	local listsSents = sg2tr.listGrossAttrChain2Sentence(listsAChains)
	print(listsSents)

	print(' >>>> regular unit test done <<<<')
end

function sg2tr.newtest()
	print(' => test useful_refs to achains')
	local lSG = sg2tr.loadSGs('./spice_sg_refs_json.json')
	local useful_listrefs = {
		{"a kitchen and dining room and living room."},
		{"the room is empty other than the furniture. ",
		"an open floor plan displays a modern kitchen, dining and living room arrangement.",
		"a kitchen, dining table and a living room looks like a small space.",
		"a small apartment is lit by modern style lamps"},
		{"two fedex trucks parked on the side of the street ."},
		{ "a couple of fed ex delivery trucks parked outside of a building.",
		"two fedex trucks parked on a side of a street with tall buidings behind them.",
		"fedex trucks parked on the side of the street while cars wait in traffic.",
		"there are a few delivery vans parked on the side of the road. "}
	}
	local useful_achains = sg2tr.sg2tr(lSG, useful_listrefs)
	print(useful_achains)

	print(sg2tr.listGrossAttrChain2Sentence(useful_achains))
end

--sg2tr.test()
--sg2tr.newtest()

return sg2tr
