phrases2sg = {}

local example_input = [[
[
  {
    "image_id": 1,
    "test": "a kitchen and dining room and living room.",
    "refs": [
      "the room is empty other than the furniture. ",
      "an open floor plan displays a modern kitchen, dining and living room arrangement.",
      "a kitchen, dining table and a living room looks like a small space.",
      "a small apartment is lit by modern style lamps"
    ]
  },
  {
    "image_id": 234,
    "test": "two fedex trucks parked on the side of the street . ",
    "refs": [
      "a couple of fed ex delivery trucks parked outside of a building.",
      "two fedex trucks parked on a side of a street with tall buidings behind them.",
      "fedex trucks parked on the side of the street while cars wait in traffic.",
      "there are a few delivery vans parked on the side of the road. "
    ]
  }
]
]]

-- @input
function phrases2sg.createJSON (image_id, test, refs, destpath)
	image_id = image_id or 1 -- int
	test = test or "" -- string
	refs = refs or {""} -- table of strings
	destpath = destpath or "/tmp/phrases2sg.json"
	local JSONObject = {image_id = image_id, test = test, refs = refs}
	local cjson = require 'cjson'
	local JSONObject_toString = cjson.encode({JSONObject})
	--print(JSONObject_toString)
	local f = io.open(destpath, 'w+')
	f:write(JSONObject_toString)
	f:flush()
	f:close()
	return JSONObject_toString
end

function phrases2sg.createBatchJSON (image_ids, tests, refss, destpath)
	image_ids = image_ids or error("image_id can't be nil")
	tests = tests or error("tests can't be nil")
	refss = refss or error("refss can't be nil")
	destpath = destpath or "/tmp/Batchphrases2sg.json"
	-- check data length
	if #image_ids ~= #tests or #image_ids ~= #refss then
		error("E: data length not match")
	end
	local Batch_JSONArray = {}
	for i = 1, #image_ids do
		local JSON_cursor = {image_id = image_ids[i], test = tests[i], refs = refss[i]}
		table.insert(Batch_JSONArray, JSON_cursor)
	end
	local cjson = require 'cjson'
	local Batch_JSONArray_toString = cjson.encode(Batch_JSONArray)
	--print(JSONObject_toString)
	local f = io.open(destpath, 'w+')
	f:write(Batch_JSONArray_toString)
	f:flush()
	f:close()
	return Batch_JSONArray_toString
end

function phrases2sg.createJSON_test ()
	local tester_image_id= 234
    local tester_test = "two fedex trucks parked on the side of the street . "
	local tester_refs = {
      "a couple of fed ex delivery trucks parked outside of a building.",
      "two fedex trucks parked on a side of a street with tall buidings behind them.",
      "fedex trucks parked on the side of the street while cars wait in traffic.",
      "there are a few delivery vans parked on the side of the road. " }
	local tester_json = phrases2sg.createJSON(tester_image_id, tester_test, tester_refs)
	print(tester_json)
	print(" -> OK")
	return true
end

function phrases2sg.createBatchJSON_test ()
	local image_ids = {}
	local tests = {}
	local refss = {}
    local tester_test = "two fedex trucks parked on the side of the street . "
	local tester_refs = {
      "a couple of fed ex delivery trucks parked outside of a building.",
      "two fedex trucks parked on a side of a street with tall buidings behind them.",
      "fedex trucks parked on the side of the street while cars wait in traffic.",
      "there are a few delivery vans parked on the side of the road. " }
	for i = 1, 10 do
		table.insert(image_ids, i)
		table.insert(tests, tester_test)
		table.insert(refss, tester_refs)
	end
	local batch_json = phrases2sg.createBatchJSON(image_ids, tests, refss)
	print(batch_json)
	print(" -> OK")
	return true
end

function phrases2sg.callSpiceSGDump(inputjson)
	os.execute("java -jar SPICE/target/spice-1.0.jar "..inputjson)
	local cjson = require 'cjson'
	-- read json files and decode
	local f = io.open("/tmp/spice_sg_test_json.json")
	local f2 = io.open("/tmp/spice_sg_refs_json.json")
	local json_raw = f:read()
	local json_raw2 = f2:read()
	f:flush()
	f2:flush()
	f:close()
	f2:close()
	local sgdump = cjson.decode(json_raw)
	local sgdump2 = cjson.decode(json_raw2)
	return {sgdump, sgdump2}
end

-- @input: phrase | a string
-- @output: sg | a scene graph
function phrases2sg.phrase2sg (test, refs)
	if test == nil and refs == nil then
		error("don't do this")
	end
	test = test or ""
	refs = refs or {""}
	local json_single_path = "/tmp/phrases2sg_single.json"
	local json_single = phrases2sg.createJSON(nil, test, refs, json_single_path)
	local sgdump_test, sgdump_refs = table.unpack(phrases2sg.callSpiceSGDump(json_single_path))
	return {sgdump_test, sgdump_refs}
end

-- @input: phrases | a table of strings
-- @output: sg | a merged scene graph
function phrases2sg.phrases2sg (test, refs)
	if test == nil and refs == nil then
		error("don't do this")
	end
	if test ~= nil and refs ~= nil then
		assert(#test == #refs) -- match size
	end
	local batchsize = nil
	if test == nil then
		test = {}
		batchsize = #refs
		for i = 1, batchsize do table.insert(test, "") end
	end
	if refs == nil then
		refs = {}
		batchsize = #test
		for i = 1, batchsize do table.insert(refs, {""}) end
	end
	if batchsize == nil then batchsize = #test end
	local image_ids = {}
	for i = 1, batchsize do table.insert(image_ids, i) end
	local json_batch_path = "/tmp/phrases2sg_batch.json"
	local json_batch = phrases2sg.createBatchJSON(image_ids, test, refs, json_batch_path)
	local sgdump_test, sgdump_refs = table.unpack(phrases2sg.callSpiceSGDump(json_batch_path))
	return {sgdump_test, sgdump_refs}
end

function phrases2sg.test ()
    local tester_test = "two fedex trucks parked on the side of the street . "
	local tester_refs = {
		"the room is empty other than the furniture. ",
		"an open floor plan displays a modern kitchen, dining and living room arran gement.",
		"a kitchen, dining table and a living room looks like a small space.",
		"a small apartment is lit by modern style lamps",
	}
	--print("I: we got "..tostring(#refphrases).." reference phrases for testing")
	
	print(" => single unit -> creat*JSON")
	io.stdout:flush()
	local _ = phrases2sg.createJSON_test() or error("ERR")
	local _ = phrases2sg.createBatchJSON_test() or error("ERR")
	print(" => single unit -> creat*JSON => OK")
	io.stdout:flush()

	print(" => single unit -> callSpiceSGDump")
	io.stdout:flush()
	local example_input_file_path = "/tmp/example_input_3434622.json"
	local example_input_file = io.open(example_input_file_path, 'w+')
	example_input_file:write(example_input)
	example_input_file:close()
	local example_sgdump = phrases2sg.callSpiceSGDump(example_input_file_path)
	print(" => single unit -> callSpiceSGDump :: test")
	io.stdout:flush()
	print(example_sgdump[1])
	print(" => single unit -> callSpiceSGDump :: refs")
	io.stdout:flush()
	print(example_sgdump[2])
	print(" => single unit -> callSpiceSGDump => OK")
	io.stdout:flush()


	--print(" => complex unit -> phrase2sg")
	--io.stdout:flush()
	--print(" => complex unit -> phrase2sg :: case0, both val")
	--io.stdout:flush()
	--local sgc0t, sgc0r = table.unpack(phrases2sg.phrase2sg(tester_test, tester_refs))
	--print(sgc0t, sgc0r)
	--print(" => complex unit -> phrase2sg :: case1 , test nil , refs val")
	--io.stdout:flush()
	--local sgc1t, sgc1r = table.unpack(phrases2sg.phrase2sg(nil, tester_refs))
	--print(sgc1t, sgc1r)
	--print(" => complex unit -> phrase2sg :: case2 , test val , refs nil")
	--io.stdout:flush()
	--local sgc2t, sgc2r = table.unpack(phrases2sg.phrase2sg(tester_test, nil))
	--print(sgc2t, sgc2r)
	--print(" => complex unit -> phrase2sg => OK")
	--io.stdout:flush()

	--print(" => complex unit -> phrases2sg")
	--io.stdout:flush()
	--print(" => complex unit -> phrases2sg :: case0, both val")
	--io.stdout:flush()
	--local sgb0t, sgb0r = table.unpack(phrases2sg.phrases2sg({tester_test, tester_test}, {tester_refs, tester_refs}))
	--print(sgb0t, sgb0r)
	--io.stdout:flush()
	--print(" => complex unit -> phrases2sg :: case1, test nil, refs val")
	--io.stdout:flush()
	--local sgb1t, sgb1r = table.unpack(phrases2sg.phrases2sg(nil, {tester_refs, tester_refs}))
	--print(sgb1t, sgb1r)
	--io.stdout:flush()
	--print(" => complex unit -> phrases2sg :: case2, test val, refs nil")
	--io.stdout:flush()
	--local sgb2t, sgb2r = table.unpack(phrases2sg.phrases2sg({tester_test, tester_test}, nil))
	--print(sgb2t, sgb2r)
	--io.stdout:flush()
	--print(" => complex unit -> phrases2sg => OK")
	--io.stdout:flush()

	print(' => dump a really useful refs json')
	local useful_listrefs = {
		{"a kitchen and dining room and living room."},
		{"the room is empty other than the furniture. ",
	      "an open floor plan displays a modern kitchen, dining and living room arrangement.",
   		   "a kitchen, dining table and a living room looks like a small space.",
   		   "a small apartment is lit by modern style lamps"},
  		{"two fedex trucks parked on the side of the street ."},
		{ "a couple of fed ex delivery trucks parked outside of a building.",
		  "two fedex trucks parked on a side of a street with tall buidings behind them.",
		  "fedex trucks parked on the side of the street while cars wait in traffic.",
		  "there are a few delivery vans parked on the side of the road. "}
	}
	sgb0t, sgb0r = table.unpack(phrases2sg.phrases2sg(nil, useful_listrefs))
	print(sgb0t, sgb0r)
	print(' => dump a really useful refs json => OK')

end

--phrases2sg.test()

return phrases2sg
