// this file comes from Stanford corenlp website
import edu.stanford.nlp.scenegraph.RuleBasedParser;
import edu.stanford.nlp.scenegraph.SceneGraph;

String sentence = "A brown fox chases a white rabbit.";

RuleBasedParser parser = new RuleBasedParser();
SceneGraph sg = parser.parse(sentence);

//printing the scene graph in a readable format
System.out.println(sg.toReadableString()); 

//printing the scene graph in JSON form
System.out.println(sg.toJSON()); 
