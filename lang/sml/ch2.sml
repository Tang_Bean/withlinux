(* Names, functions and types
   Chapter 2 http://www.cl.cam.ac.uk/~lp15/MLbook/PDF/chapter2.pdf
 *)

val seconds = 60 : int;

val pi = 3.1415926; (* real *)

fun area r = pi * r * r; (* parantheses are optional *)

area 1.0;

(* fun square x = x * x; will cause error, can't resolve overloading for * *)
fun square (x: real) = x * x;

size("asdf"); (* 4, size("") is 0 *)

#"a"; (* constant *)
chr; (* int -> char *)
ord #"0"; (* char -> int *)

fun sign (n) =
    if n > 0 then 1
    else if n = 0 then 0
    else (* n < 0 *) ~1;

fun isLower c = #"a" <= c andalso c <= #"z";

val zerovec = (0.0, 0.0); (* real * real *)

fun lengthvec (x, y) = Math.sqrt(x*x + y*y); (* fn: real * real -> real *)

lengthvec zerovec;

lengthvec (1.0, 1.0);

fun negvec (x, y) : real*real = (~x, ~y);

val bn = negvec(zerovec);

type vec = real * real;

((0,0), (0,0)); (* (int*int)*(int*int) *)

fun addvec ((x1, y1), (x2, y2)) : vec = (x1+x2, y1+y2);

val ((x1, y1), (x2, y2)) = (addvec(zerovec, zerovec), addvec(zerovec, zerovec));

val henryV = {
    name = "Henry V",
    born = 1387,
    died = 1422 };

val {name, born, died} = henryV;

#born henryV; (* Record field selection *)

#2 ("a", "b", 3, false); (* "b" *)

type king = { name : string,
              born : int,
              died : int };

fun lifetime (k: king) = #died k - #born k;

lifetime henryV;

infix xor;
fun (p xor q) = (p orelse q) andalso not (p andalso q);
true xor false;

(* 1+~3: error, unknown +~; write 1+ ~3 instead *)

(* SML evaluation rule: call-by-value, instead of (lazy) call-by-need *)

fun fact n =
    if n = 0 then 1 else n * fact(n-1); (* recursive version *)

fun facti (n, p) =
    if n = 0 then p else facti(n-1, n*p); (* iterative/ tail recursive version *)

fun cond (p, x, y) : int = if p then x else y;

fun gcd(m, n) =
    if m=0 then n
        else gcd(n mod m, m);

fun power(x, k) : real =
    if k=1 then x
    else if k mod 2 = 0 then   power(x*x, k div 2)
                        else x*power(x*x, k div 2);

Math.pow(2.0, 10.0);

fun nextfib(prev, curr : int) = (curr, prev+curr);

fun fibpair(n) = 
    if n = 1 then (0, 1) else nextfib(fibpair(n-1));

fun itfib(n, prev, curr) : int =
    if n=1 then curr
    else itfib(n-1, curr, prev+curr);

fun increase(k, n) = if (k+1)*(k+1) > n then k else k+1;

fun introot n =
    if n=0 then 0 else increase(2 * introot(n div 4), n);

fun fraction(n, d) =
    let val com = gcd(n,d)
    in (n div com, d div com)
    end; (* let ... in ... end : local declaration *)

fun findroot (a, x, acc) =
    let val nextx = (a/x + x) / 2.0
    in if abs(x-nextx) < acc*x
        then nextx else findroot(a, nextx, acc)
    end;

signature ARITH = sig
    type t
    val zero : t
    val sum  : t * t -> t
    val diff : t * t -> t
    val prod : t * t -> t
    val quo  : t * t -> t
end;

structure Complex = struct
    type t = real * real;
    val zero = (0.0, 0.0);
    fun sum((x, y), (x', y')) = (x+x', y+y') : t;
    fun diff((x, y), (x', y')) = (x-x', y-y') : t;
    fun prod((x, y), (x', y')) = (x*x' - y*y', x*y'+x'*y) : t;
    fun recip(x,y) = let val t = x*x + y*y
                     in (x/t, ~y/t) end
    fun quo(z, z') = prod(z, recip z');
end;

local
  val i = (0.0, 1.0);
  val a = (0.3, 0.0);
  val b = Complex.sum(a, i);
in
  val c = Complex.sum(b, (0.7, 0.0));
  val d = Complex.prod(b, b);
end;

(*
* a variable stands for a value, it can be redeclared but not updated.
* basic values have type int, real, char, string or bool.
* values of any types can be combined to form tuples and records.
* numerical operations can be expressed as recursive functions.
* an iterative function employs recursion in a limited fashion, where
  recursive calls are essentially jumps.
* structures and signatures serve to organize large programs.
* a polymorphic type is a cheme containing type variables.
*)

print "ch2 done\n";
