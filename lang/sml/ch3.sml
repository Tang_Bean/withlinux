(* Lists
   http://www.cl.cam.ac.uk/~lp15/MLbook/PDF/chapter3.pdf *)

val l = nil; (* nil is equivalent to [] *)

val l = 9 :: l; (* list insert *)

val l2 = [ Math.sin 0.5, Math.cos 0.5, Math.exp 0.5 ];

fun upto (m, n) = (* constructing [m, m+1, ..., n] *)
  if m>n then [] else m :: upto(m+1, n);

fun prodof3 [i,j,k] : int = i * j * k;

fun prod []      = 1
  | prod (n::ns) = n * (prod ns); (* fn: int list -> int *)

prod[2,3,5];

fun maxl [m] : int  = m
  | maxl (m::n::ns) = if m>n then maxl(m::ns) else maxl(n::ns);

(* maxl[]; Exception: Match *)

fun factl (n) = prod(upto(1,n));

explode("asdf"); (* string -> list char *)

implode[#"a", #"s", #"d", #"f"]; (* list char -> string *)

concat["asdf", "asdf"]; (* list string -> string *)

fun null []     = true
  | null (_::_) = false; (* _ is the wildcard pattern *)

fun hd (x::_) = x; (* no pattern for []. It is a partial function *)

fun tl (_::xs) = xs;

fun nlength []      = 0
  | nlength (x::xs) = 1 + nlength xs; (* naive, wasteful *)

local
  fun addlen (n, []) = n
    | addlen (n, x::l) = addlen(n+1, l)
in
  fun length l = addlen(0, l)
end;

fun take ([], i) = []
  | take (x::xs, i) = if i>0 then x::take(xs, i-1) else [];

fun rtake ([], _, taken)    = taken
  | rtake (x::xs, i, taken) = if i>0 then rtake(xs, i-1, x::taken)
                                     else taken;

fun drop ([], _) = []
  | drop (x::xs, i) = if i>0 then drop(xs, i-1) else x::xs;

[1,2,3]@[4,5,6]; (* append *)

fun nrev [] = []
  | nrev (x::xs) = (nrev xs) @ [x]; (* grossly inefficient *)

rev[1,2,3,4,5];

fun concat [] = []
  | concat (l::ls) = l @ concat ls;

fun zip(x::xs, y::ys) = (x,y) :: zip(xs, ys)
  | zip _ = [];

fun unzip [] = ([], [])
  | unzip ((x,y)::pairs) =
    let val (xs, ys) = unzip pairs
    in (x::xs, y::ys) end;

fun revunzip ([], xs, ys) = (xs, ys)
  | revunzip((x,y)::pairs, xs, ys) = revunzip(pairs, x::xs, y::ys);

fun makingchange (coinvals, 0) = []
  | makingchange (c::coinvals, amount) =
    if amount<c then makingchange(coinvals, amount)
        else c::makingchange(c::coinvals, amount-c);

(* p. 16 *)
