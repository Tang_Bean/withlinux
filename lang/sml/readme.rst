Standard ML Snippets
====================

Standard ML is a functional programming language with some **impure** features.

Package
-------

mlton, smlnj

References
----------

1. http://www.cl.cam.ac.uk/~lp15/MLbook/pub-details.html

2. http://www.cl.cam.ac.uk/~lp15/MLbook/programs/

3. https://learnxinyminutes.com/docs/standard-ml/

4. https://en.wikipedia.org/wiki/Standard_ML

5. https://en.wikibooks.org/wiki/Standard_ML_Programming

6. http://homepages.inf.ed.ac.uk/stg/NOTES/notes.pdf
