# @file diudc.py
import os
import shlex
import sys

''' Core Abstraction
all the member functions are very basic definitions or just stub.
'''
class DIUDC(object):
    def __init__(self):
        # internal status involves:
        # DC: different tasks
        # DJ: different command lines generated from template
        # DD: do the jobs in serial or in parallel?
        #     's' for serial
        #     'p' for parallel
        # DT: task type
        #     'c' for simple command template
        #     't' for file or directory templates
        self.DC = {}
        self.DJ = []
        self.DD = 's' # default: serial
        self.DT = ''
        return self
    def loadtemplate(self):
        # load template from a string for cmd, or a file or directory
        raise Exception("E: please override {}.loadtemplate", super.__name__)
    def populate(self):
        # populate command lines and files from given templates
        raise Exception("E: please override {}.populate", super.__name__)
    def pre(self):
        # pre-run hooks
        raise Exception("E: please override {}.pre", super.__name__)
    def doit(self):
        # run your task under different configurations
        raise Exception("E: please override {}.doit", super.__name__)
    def post(self):
        # post-run hooks
        raise Exception("E: please override {}.post", super.__name__)
    def fire(self):
        # default pipeline
        self.populate()
        self.pre()
        self.doit()
        self.post()

''' Base DIUDC
Base DIUDC handles argument list with care.
'''
class DiudcBase(DIUDC):
    def __init__(self, dc = [], dt = 'c'):
        DIUDC.__init__(self)
        self.DC = dc
        self.DT = dt
        return self
    def loadtemplate(self, dt, template):
        if dt == 'c':
            self.DT = 'c'
        elif dt == 't':
            self.DT = 't'
        else:
            raise Exception("E: Invliad template type")

    def populate(self):
        # generate task replacement proposal
        if isinstance(self.DC, list) or isinstance(self.DC, tuple):
            # simple one-placeholder replacement
            self.DCP = [ [('#DIUDC#', dc) for dc in self.DC] ]
        elif isinstance(self.DC, dict):
            # generate different combinations
            raise Exception("not implemented")
        else:
            raise Exception("E: invalid dt type")
        # detect task type { cmd, template }
        if self.DT == 'c':
            # generate different command lines and put them into self.DJ
            pass

from subprocess import call

class DIUDC(_DIUDC_):
    def __init__(self, dc = []):
        _DIUDC_.__init__(self)
        self.DC = dc
    def populate(self):
        self.DJ = [['gcc', '-o', 'a', 'a.c']+dc.split() for dc in self.DC]
        print(self.DJ)
    def pre(self):
        pass
    def doit(self):
        for job in self.DJ:
            print(job)
            print(call(job))
    def post(self):
        pass

if __name__=='__main__':
    diudc = DIUDC().loadcmd("gcc -o a a.c #DIUDC#")
    diudc.loaddc(["-O3", "-O2", "-O2 -march=native"])
    diudc.fire()
