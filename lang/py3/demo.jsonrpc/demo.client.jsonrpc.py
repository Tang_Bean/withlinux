# https://github.com/pavlov99/json-rpc
# https://github.com/pavlov99/json-rpc/blob/master/examples/client.py
# https://en.wikipedia.org/wiki/JSON-RPC
import requests
import json
from pprint import pprint as pdump

def main():
    url = 'http://localhost:4000/'
    headers = {'content-type': 'application/json'}

    # example echo method
    payload = {
            'method': 'echo',
            'params': ['echo me!'],
            'jsonrpc': '2.0',
            'id': 0,
            }
    pdump(payload)
    response = requests.post(url, data=json.dumps(payload),
            headers=headers).json()
    pdump(response)

    assert(response['result'] == 'echo me!')
    assert(response['jsonrpc'])
    assert(response['id'] == 0)

    # example add method
    payload = {
            'method': 'add',
            'params': [1, 2],
            'jsonrpc': '2.0',
            'id': 1,
            }
    pdump(payload)
    response = requests.post(url, data=json.dumps(payload),
            headers=headers).json()
    pdump(response)

    # example foobar method
    payload = {
            'method': 'foobar',
            'params': {'foo': 'json', 'bar': '-rpc'},
            'jsonrpc': '2.0',
            'id': 3
            }
    pdump(payload)
    response = requests.post(url, data=json.dumps(payload),
            headers=headers).json()
    pdump(response)


if __name__ == '__main__':
    main()
