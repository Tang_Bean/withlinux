LANGUAGE
========

* Important Languages to me::

  Python3         *****
  C++              ****
  C                ****
  Julia            ****
  CUDA               **
  Lua                **
  Shell               *
  Matlab/Octave       *

* I want to learn more about::

  Julia           ***** (LLVM IR)
  CUDA              ***
  Cython            ***
  Python3           ***
  Go                 **
  Lisp                *

TOOL LANGUAGE
=============

* Most important tool languages for me::

  ReST            *****
  Tex             *****
  Markdown          ***
  Awk                **
  Sed                **
  CMake              **
  Make               **
