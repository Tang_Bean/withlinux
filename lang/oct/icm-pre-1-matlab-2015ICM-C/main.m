%!/usr/bin/matlab
%% main.m 2015
close all;

%% configuration
% const
CF_N = 370;
CF_N1 = 290;
CF_N2 = 70;
CF_N3 = 10;
% param
alpha = 0.001;
gamma = 0.001;
omega = 0.00000667;

%% initialize
LDB ('');
LDB ('start');
% the adjacent matrix
%adjacent = zeros(CONF_POSITIONS, CONF_POSITIONS);
adjacent0 = round(rand(370)/10);

adjacent = adjacent0;
maskocp = zeros(1,370);
maskocp = incvectorfiller (maskocp, floor(0.85*370))

%% start iter
h1_0 = 290*0.15;
h2_0 = 70*0.15;
h3_0 = 10*0.15;
h1 = [ h1_0, zeros(1,24)];
h2 = [ h2_0, zeros(1,24)];
h3 = [ h3_0, zeros(1,24)];
cc = zeros(1,25);

LDB ('start iter');

for i = 2:25
    %cc(i) = cc_with_mask (maskocp, adjacent);
    h1(i) = h1(i-1)/3 + churnbeta(maskocp, adjacent, i, 1, omega, alpha, gamma)*h1(i-1)*(290-h1(i-1));
    h2(i) = h2(i-1)/3 + churnbeta(maskocp, adjacent, i, 2, omega, alpha, gamma)*h2(i-1)*(70 -h2(i-1));
    h3(i) = h3(i-1)/3 + churnbeta(maskocp, adjacent, i, 3, omega, alpha, gamma)*h3(i-1)*(10 -h3(i-1));
   
    L1 = 290*ones(1,25);
    L2 = 70 *ones(1,25);
    L3 = 10 *ones(1,25);
    L1 = round(L1 - h1);
    L2 = round(L2 - h2);
    L3 = round(L3 - h3);
    
    
    maskocp = incvectorfiller (maskocp, L1(i) + L2(i) + L3(i))
    
end


figure;
hold on;
grid on;
plot (L1, 'b');
plot (L2, 'r');
plot (L3, 'g');
legend ('L1', 'L2', 'L3');

cc
