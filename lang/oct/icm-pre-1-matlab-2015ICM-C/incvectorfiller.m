% incremental vector filler
function next = incvectorfiller (prev, num);
[sizei, sizej] = size(prev);
if 1 ~= sizei
    LDB (' not a row vector');
end
next = prev;
current = sum(prev ~= 0);
if abs(num-current) > sizej
    LDB ('invalid num');
    %return;
end
if num - current >= 0
    LDB ('above');
    num-current
    for i = 1:(num-current)
        cur = ceil( sizej * rand());
        while next(cur) ~= 0
            cur = ceil (sizej * rand());
        end
        next (cur) = 1;
    end
else
    LDB ('below');
    num-current
    for i = 1:abs(num-current)
        cur = ceil (sizej * rand());
        while next (cur) == 0
            cur = ceil (sizej * rand());
        end
        next (cur) = 0;
    end
end

end