%% churn beta
function beta = churnbeta (maskocp, adjacent, t, class, sigma, alpha, gamma)
if 1 == class
    %beta = sigma * t * (alpha * 8 + gamma * k);
    k = 0;
    for i=1:10
        if maskocp(i) ~=0
            k = k+sum(adjacent(i,:));
        end
    end
    k = k/10;
    beta = sigma * t * (alpha * 8 + gamma * k);
elseif 2 == class
    k = 0;
    for i=11:80
        if maskocp(i) ~=0
            k = k+sum(adjacent(i,:));
        end
    end
    k = k/70;
    beta = 2*sigma*t * (alpha * 2.3929 + gamma * k);
elseif 3 == class
    %beta = sigma * t * (alpha * 0.93793 + gamma *k);
    k = 0;
    for i=81:370
        if maskocp(i) ~=0
            k = k+sum(adjacent(i,:));
        end
    end
    k = k/290;
    beta = sigma * t * (alpha * 0.93793 + gamma * k);
else
    LDB ('invalid class for churnbeta');
    beta = nan;
end
beta
end