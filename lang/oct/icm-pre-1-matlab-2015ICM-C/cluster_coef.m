% calculate the clustering coefficient

% input is adjacent matrix, where adj[i][j] \in {0,1}
function c_coef = cluster_coef (adjacent)
[ sizei, sizej ] = size (adjacent);

% check square matrix
if sizei - sizej ~= 0 || sizei < 3
    disp (sprintf ('invalid input'));
    c_coef = 0;
end
% check value range
if max(max(adjacent)) > 1 || min(min(adjacent)) < 0
    disp (sprintf ('invalid input'));
    c_coef = 0;
end

% find triangles and triplets
combinations = combntns ([1:sizei], 3);

triangles = 0;
triplets = 0;

edge = 0;
disp (sprintf ('I: %d combinations', length(combinations)));
for N = 1:length(combinations)
    i = combinations(N, 1);
    j = combinations(N, 2);
    k = combinations(N, 3);
    % next if i = j or j = k or k = i
    if (i==j) || (j==k) || (k==i)
       continue;
    end
    % count edges
    edge = 0;
    if 1==adjacent(i,j)
       edge = edge + 1;
    end
    if 1==adjacent(j,k)
        edge = edge + 1;
    end
    if 1==adjacent(i,k)
    	edge = edge + 1;
    end
    % update triangle and triplets counter
    if 2 == edge
	    triplets = triplets + 1;
    elseif 3 == edge
    	triplets = triplets + 3;
    	triangles = triangles + 1;
    else
    	;
    end
end

% output cluster coef
c_coef = triangles / triplets;
c_coef = 3 * c_coef;

end
