% main for Q2

close all;

debug_from = 1

load orig2
load input2

input_sequence = input2
%
%iv2 = []
%for i = 2649:-1:1
%iv2 = [ iv2, input2(:,i) ];
%end
%input_sequence = iv2;
%
%t = (-1).^(0:1000);
%input_sequence = [ 100*cos((0:.1:100)/10).*t; 100*[0:.1:100]; 100*[0:.1:100] ];
input_is_cos = 0

%input_sequence = rand(3,1000);

figure
plot3 (input_sequence(1,:), input_sequence(2,:), input_sequence(3,:), 'o');

[ii, ij] = size(input_sequence);

label = []
labelstack = []
stack1 = []
stack2 = []
%threshold = 100
threshold = 0

trace1 = figure 
trace2 = figure
%trace = figure

pause

for n = 1:ij
	if n == 1
		threshold = 0
	else
		if input_is_cos == 1
			threshold = 10
		else
		%threshold = 400 * (time2(n) - time2(n-1))
			threshold = 400
		end
	end
	[label, stack1, stack2] = moclass (input_sequence(:,n), stack1, stack2, threshold);
	labelstack = [labelstack, label];

	draw = 1;
	if n > debug_from
		[di1,dj1] = size(stack1);
		[di2,dj2] = size(stack2);
		if (draw == 1)&(dj1>400)&(dj2>400)
			figure(trace1)
			%subplot(1,2,1);
			plot3(stack1(1,dj1-400:dj1), stack1(2,dj1-400:dj1), stack1(3,dj1-400:dj1), 'ob')
			grid on
			figure(trace2)
			%subplot(1,2,2);
			%plot3(stack2(1,:), stack2(2,:), stack2(3,:), 'or')
			plot3(stack2(1,dj2-400:dj2), stack2(2,dj2-400:dj2), stack2(3,dj2-400:dj2), 'or')
			grid on
		elseif (draw == 1)&(dj1>0)&(dj2>0)
			figure(trace1)
			%subplot(1,2,1);
			plot3(stack1(1,:), stack1(2,:), stack1(3,:), 'ob')
			grid on
			figure(trace2)
			%subplot(1,2,2);
			plot3(stack2(1,:), stack2(2,:), stack2(3,:), 'or')
			grid on
		end
		disp ('------------------------------------------------');
		n
		%pause
	end
end

figure(trace1)
plot3(stack1(1,:), stack1(2,:), stack1(3,:), 'ob')
grid on
figure(trace2)
plot3(stack2(1,:), stack2(2,:), stack2(3,:), 'or')
grid on
