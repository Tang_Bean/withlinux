function output = a2r (input)
output = input * pi / 180;