function [ label, stack1_next, stack2_next ] = moclass (point, stack1, stack2, threshold)
% momentum-based classifier
% Copyright (C) 2015 Zhou Mo

% point is column point
% stack is for saving points in the same class
% threshold is somewhat a coef. 

tail = 0;

% first save stack
stack1_next = stack1;
stack2_next = stack2;

[i1,j1] = size(stack1);
[i2,j2] = size(stack2);


if length(stack1) ~= 0
	st1last = stack1(:,j1)
end
if length(stack2) ~= 0
	st2last = stack2(:,j2)
end

if (j1 == 0) & (j2 == 0)
% put it in the 1 class then return
	point
	disp ('both 0 len');
	disp ('was put in the 1st class');
	label = 1;
	stack1_next = [ stack1_next, point ];
	return;
elseif (length(stack1) ~= 0) & (length(stack2) == 0)
	disp ('1len ~=0 and 2len == 0');
	point
	if norm(point - st1last) >= threshold
		disp ('diff norm geq thres');
		disp ('was put in the 2nd class');
		label = 2;
		stack2_next = [ stack2_next, point];
		return;
	else
		disp ('diff norm le thres');
		disp ('was pu in the 1st class');
		label = 1;
		stack1_next = [ stack1_next, point];
		return;
	%if norm(point - st1last) >= norm(point - st2last)
	%	disp ('diff norm geq thres');
	%	disp ('was put in the 2nd class');
	%	label = 2;
	%	stack2_next = [ stack2_next, point];
	%	return;
	%elseif norm(point - st1last) < norm(point - st2last)
	%	disp ('diff norm le thres');
	%	disp ('was pu in the 1st class');
	%	label = 1;
	%	stack1_next = [ stack1_next, point];
	%	return;
	end
else
	disp ('1len and 2len neither 0');
	point
	thresdec = 0;
	modec = 0;
	nedev = 0;
	dist1 = norm(point - st1last);
	dist2 = norm(point - st2last);
	% threshold decision
	if (dist1 - dist2 > threshold)
		disp ('* try: thres');
		disp ('diff dist 1 2 > thres');
		disp ('2nd class');
		%label = 2;
		%stack2_next = [ stack2_next, point];
		%return;
		thresdec = 2;
	elseif (dist2 - dist1 > threshold)
		disp ('* try: thres');
		disp ('diff dist 2 1 > thres');
		disp ('1st class');
		%label = 1;
		%stack1_next = [ stack1_next, point];
		%return;
		thresdec = 1;
	elseif ((dist1>threshold)&(dist2<threshold))
		disp ('* try: thres');
		disp ('d1 > thres and d2 < thres');
		disp ('2nd class');
		%label = 2;
		%stack2_next = [ stack2_next, point];
		%return;
		thresdec = 2;
	elseif ((dist1<threshold)&(dist2>threshold))
		disp ('* try: thres');
		disp ('d1 < thres and d2 > thres');
		disp ('1nd class');
		%label = 1;
		%stack1_next = [ stack1_next, point];
		%return;
		thresdec = 1;
%	elseif (dist1 - dist2 > threshold)
%		disp ('try: thres');
%		disp ('diff dist 1 2 > thres');
%		disp ('2nd class');
%		%label = 2;
%		%stack2_next = [ stack2_next, point];
%		%return;
%		thresdec 
%	elseif (dist2 - dist1 > threshold)
%		disp ('try: thres');
%		disp ('diff dist 2 1 > thres');
%		disp ('1st class');
%		label = 1;
%		stack1_next = [ stack1_next, point];
%		return;
%	elseif (dist1 > dist2/2)
%		disp ('try: thres');
%		disp ('diff dist 1 2 > thres');
%		disp ('2nd class');
%		label = 2;
%		stack2_next = [ stack2_next, point];
%		return;
%	elseif (dist2 > dist1/2)
%		disp ('try: thres');
%		disp ('diff dist 2 1 > thres');
%		disp ('1st class');
%		label = 1;
%		stack1_next = [ stack1_next, point];
%		return;
	end
	% then try momentum
	if (j1 > 1) & (j2 > 1)
		disp ('* try: moment');
		% calc momentum
		disp ('momentum judge');
		% judge by momentum
		vec1 = point - st1last;
		vec2 = point - st2last;
		vec1old = st1last - stack1(:,j1-1);
		vec2old = st2last - stack2(:,j2-1);
		for m = 2:50
			if (j1 > m)&(j2 > m)
				vec1old = vec1old + stack1(:,j1-(m-1)) - stack1(:,j1-m);
				vec2old = vec2old + stack2(:,j2-(m-1)) - stack2(:,j2-m);
				tail = tail + 1;
			end
		end
		vec1
		vec1old
		vec2
		vec2old
		tail
		tail = 0;

		% cosine distance
		cosd1 = (vec1' * vec1old)/(norm(vec1)*norm(vec1old))
		cosd2 = (vec2' * vec2old)/(norm(vec2)*norm(vec2old))
		if cosd1 > cosd2
			disp ('set to class 1');
			%label = 1;
			%stack1_next = [ stack1_next, point ];
			%return;
			modec = 1;
		elseif cosd1 < cosd2
			disp ('set to class 2');
			%label = 2;
			%stack2_next = [ stack2_next, point ];
			%return;
			modec = 2;
		end	
	end
	if 1
	% finally found neighbour
		disp ('* try: near');
		disp ('because cannot calc momentum');
		disp ('still dist judge, without thres');
		nei_counter = 0;
		nei_center1 = zeros(3,1);
		nei_center2 = zeros(3,1);	
		for m = 1:50
			if (j1>m)&(j2>m)
				nei_center1 = nei_center1 + stack1(:,j1 - (m - 1));
				nei_center2 = nei_center2 + stack2(:,j2 - (m - 1));
				nei_counter = nei_counter + 1;
			end
		end
		nei_center1 = nei_center1 / nei_counter
		nei_center2 = nei_center2 / nei_counter
		dist1 = norm(point - nei_center1)
		dist2 = norm(point - nei_center2)
		%dist1 = norm(point - st1last);
		%dist2 = norm(point - st2last);
		if dist1 - dist2 >= 0
			disp ('near');
			disp ('2nd class');
			%label = 2;
			%stack2_next = [ stack2_next, point];
			%return;
			nedec = 2;
		elseif dist2 - dist1 >= 0
			disp ('near');
			disp ('1st class');
			%label = 1;
			%stack1_next = [ stack1_next, point];
			%return;
			nedec = 1;
		else
			nedec = 0;
		end
	end
	disp ('* I: begin to decide');
	thresdec
	modec
	nedec
	dec = [thresdec, modec, nedec];
	if sum(dec == 1) > 2
		% all 1
		disp ('decide 1');
		label = 1;
		stack1_next = [ stack1_next, point];
		return;
	elseif sum(dec == 2) > 2
		% all 2
		disp ('decide 2');
		label = 2;
		stack2_next = [ stack2_next, point];
		return;
	else
		% other	
		% first thres
		%if thresdec == 1
		%	disp ('decide 1');
		%	label = 1;
		%	stack1_next = [ stack1_next, point];
		%	return;
		%elseif thresdec == 2
		%	disp ('decide 2');
		%	label = 2;
		%	stack2_next = [ stack2_next, point];
		%	return;
		%end
		% then count
		if sum(dec==1) > sum(dec==2)
			disp ('decide 1');
			label = 1;
			stack1_next = [ stack1_next, point];
			return;
		elseif sum(dec==1) < sum(dec==2)
			disp ('decide 2');
			label = 2;
			stack2_next = [ stack2_next, point];
			return;
		else
			disp ('how to decide !!!!!!!!!');
			%if (modec == 1)
			%	disp ('decide 1');
			%	label = 1;
			%	stack1_next = [ stack1_next, point];
			%	return;
			%elseif modec == 2
			%	disp ('decide 2');
			%	label = 2;
			%	stack2_next = [ stack2_next, point];
			%	return;
			%end
			disp (' -> calc decision coef');
			diffcosd = abs(cosd1-cosd2);
			%decoef_cosd = ( abs(diffcosd/cosd1) + abs(diffcosd/cosd2) )/2
			decoef_cosd = diffcosd / max(abs(cosd1),abs(cosd2));
			diffdist = abs(dist1-dist2);
			%decoef_dist = ( abs(diffdist/dist1) + abs(diffdist/dist2) )/2
			decoef_dist = diffdist / max(abs(dist1),abs(dist2));
			if decoef_cosd > decoef_dist
				if modec  == 1
					disp ('final decide 1');
					label = 1;
					stack1_next = [ stack1_next, point];
					return;
				elseif modec == 2
					disp ('final decide 2');
					label = 2;
					stack2_next = [ stack2_next, point];
					return;
				end
			else
				if nedec  == 1
					disp ('final decide 1');
					label = 1;
					stack1_next = [ stack1_next, point];
					return;
				elseif nedec == 2
					disp ('final decide 2');
					label = 2;
					stack2_next = [ stack2_next, point];
					return;
				end
			end
		end
	end
end
disp ('final decision')
label
