function [x, y, z] = sph2xyz (distance, direction, pitch)
% convert sphere coord system to xyz coord system

%y = distance .* cos(a2r(pitch)) .* sin(a2r(direction));
%x = distance .* cos(a2r(pitch)) .* cos(a2r(direction));
%z = distance .* sin(a2r(pitch));

[x,y,z] = sph2cart (a2r(pitch), a2r(direction), distance);
