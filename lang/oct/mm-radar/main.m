
load orig1

%xbase1 = 6400*1000 * cos(a2r(40.5)) * sin(a2r(122.1))
%ybase1 = 6400*1000 * cos(a2r(40.5)) * cos(a2r(122.1))
%zbase1 = 6400*1000 * sin(a2r(40.5))
%
%xbase2 = 6400*1000 * cos(a2r(41.5)) * sin(a2r(122.4))
%ybase2 = 6400*1000 * cos(a2r(41.5)) * cos(a2r(122.4))
%zbase2 = 6400*1000 * sin(a2r(41.5))
%
%xbase3 = 6400*1000 * cos(a2r(41.9)) * sin(a2r(122.7))
%ybase3 = 6400*1000 * cos(a2r(41.9)) * cos(a2r(122.7))
%zbase3 = 6400*1000 * sin(a2r(41.9))

%ybase1 = 6400*1000 * cos(a2r(40.5)) * sin(a2r(122.1))
%xbase1 = 6400*1000 * cos(a2r(40.5)) * cos(a2r(122.1))
%zbase1 = 6400*1000 * sin(a2r(40.5))
%
%ybase2 = 6400*1000 * cos(a2r(41.5)) * sin(a2r(122.4))
%xbase2 = 6400*1000 * cos(a2r(41.5)) * cos(a2r(122.4))
%zbase2 = 6400*1000 * sin(a2r(41.5))
%
%ybase3 = 6400*1000 * cos(a2r(41.9)) * sin(a2r(122.7))
%xbase3 = 6400*1000 * cos(a2r(41.9)) * cos(a2r(122.7))
%zbase3 = 6400*1000 * sin(a2r(41.9))

[xbase1,ybase1,zbase1] = sph2cart(a2r(122.1), a2r(40.5), 6400*1000)
[xbase2,ybase2,zbase2] = sph2cart(a2r(122.4), a2r(41.4), 6400*1000)
[xbase3,ybase3,zbase3] = sph2cart(a2r(122.7), a2r(41.9), 6400*1000)

%[x1, y1, z1 ] = sph2xyz (dist1, dire1, high1)
[x1, y1, z1] = sph2cart (high1, dire1, dist1)
%figure
%plot3 (x1, y1, z1, 'o')

longi = (num1==1).*122.1 + ...
(num1 ==2) .*122.4 + ...
(num1==3).*122.7

lati = ...
(num1==1).*40.5 + ...
(num1==2).*41.5 + ...
(num1==3).*41.9

xbase = ...
(num1==1).* xbase1 + ...
(num1==2).* xbase2 + ...
(num1==3).* xbase3

ybase = ...
(num1==1).* ybase1 + ...
(num1==2).* ybase2 + ...
(num1==3).* ybase3


zbase = ...
(num1==1).* zbase1 + ...
(num1==2).* zbase2 + ...
(num1==3).* zbase3

[xu, yu, zu ]  = radar_coord_conv (xbase, ybase, zbase, longi, lati, x1, y1, z1)
figure
plot3 (xu, yu, zu , 'o')
