%!/usr/bin/octave

% grub.m
% first sed all data.

files = dir('data');
[x,y] = size(files);

disp ('Total number of files:');
disp (x);

counter = 1;
for i = 1:x
    % skip invalid files
    if strcmp(files(i).name, '.') | strcmp(files(i).name, '..')
        continue;
    end
    path = strcat('data/', files(i).name)
    data(counter).name = files(i).name;
    [data(counter).id, data(counter).lat, data(counter).lon] = ...
        textread (path,'%n%*s%f%f','delimiter',',')
    %[a,b,c,d] = [textread( strcat('data/',files(i).name),'%n %s %f %f\n','delimiter',',')];
    counter = counter + 1;
end