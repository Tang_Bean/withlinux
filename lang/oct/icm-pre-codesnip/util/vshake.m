function next = vshake (prev, incre)
% shake vector of bits
% prev is a vector X, where x_i \in {0,1}
% incre is the increament number of bit 1
% Zhou Mo, 2016
next = prev;
[I,J] = size (prev);
if ~((I==1)||(J==1))
	disp ('.vshake: illegal input');
	return;
end
N = length(prev);
for i=1:N
	if ~((prev(i)==1)||prev(i)==0)
		disp ('.vshake: illegal bit');
	end
end
if incre == 0
	next = prev;
	return;
else if incre > 0
	times = incre;
	while times > 0
		cur = ceil(length(prev)*rand(1));
		if sum(next) >= length(next)
			return;
		end
		while (next(cur) == 1)&&(cur ~= 0)
			% move cursor to a '0' bit
			cur = ceil(length(prev)*rand(1));
		end
		next(cur) = 1;
		times = times -1;
	end
else % if incre < 0
	times = abs(incre);
	while times > 0
		cur = ceil(length(prev)*rand(1));
		if sum(next) == 0
			return;
		end
		while (next(cur) == 0)&&(cur ~= 0)
			cur = ceil(length(prev)*rand(1));
		end
		next(cur) = 0;
		times = times -1;
	end
end

end % function vshake
