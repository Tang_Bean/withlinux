fmincon() : nonlinprog

syms x y 
f = x^3 + y^3
df = jacobian (f)

---
0-constrain

solve()

fminunc()
fminsearch()

f  = @(x) x^2

fsolve()

---
quadritic prog

quadprog()

---
罚函数法，非线性规划到无约束极值问题

---
约束min/max

1. fminbnd()
2. fsemif()
3. fminmax()
4. gradient

---
optimtool
