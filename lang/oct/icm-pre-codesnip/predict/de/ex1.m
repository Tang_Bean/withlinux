% http://baike.baidu.com/subview/3918128/3918128.htm
odefun=@(t,y) (y+3*t)/t^2; %定义函数
tspan=[1 4]; %求解区间
y0=-2; %初值
[t,y]=ode45(odefun,tspan,y0);
plot(t,y) %作图
title('t^2y''=y+3t,y(1)=-2,1<t<4')
legend('t^2y''=y+3t')
xlabel('t')
ylabel('y')
% 精确解
% dsolve('t^2*Dy=y+3*t','y(1)=-2')
% ans =一阶求解结果图
% (3*Ei(1) - 2*exp(1))/exp(1/t) - (3*Ei(1/t))/exp(1/t)
