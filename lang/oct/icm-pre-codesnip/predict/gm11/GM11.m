function [pred, next] = GM11(X0)
%format long ;
% 灰色模型只适合指数增长的数据
[m,n]=size(X0);

X1=cumsum(X0);   %累加

X2=[];
for i=1:n-1
    X2(i,:)=X1(i)+X1(i+1);
end
B=-0.5.*X2 ;
t=ones(n-1,1);
B=[B,t]  ;      % 求B矩阵

YN=X0(2:end)  ;
P_t=YN./X1(1:(length(X0)-1)) %对原始数据序列X0进行准光滑性检验，
                            %序列x0的光滑比P(t)=X0(t)/X1(t-1)
A=inv(B.'*B)*B.'*YN.' ;
a=A(1) 
u=A(2) 
c=u/a  ;
b=X0(1)-c ;
 X=[num2str(b),'*exp','(',num2str(-a),'*k',')',num2str(c)];
 strcat('X(k+1)=',X)
 %syms k;
 for t=1:length(X0)
     k(1,t)=t-1;
 end
k
Y_k_1=b*exp(-a*k)+c;
Y = [];
for j=1:length(k)-1
   Y(1,j)=Y_k_1(j+1)-Y_k_1(j);
end
XY=[Y_k_1(1),Y]    %预测值
pred = XY;

CA=abs(XY-X0) ;    %残差数列
Theta=CA       %残差检验 绝对误差序列
XD_Theta= CA ./ X0   %残差检验 相对误差序列
AV=mean(CA);       % 残差数列平均值
 
R_k=(min(Theta)+0.5*max(Theta))./(Theta+0.5*max(Theta)) ;% P=0.5
R=sum(R_k)/length(R_k)  %关联度

Temp0=(CA-AV).^2 ;
Temp1=sum(Temp0)/length(CA);
S2=sqrt(Temp1) ;    %绝对误差序列的标准差
%----------
AV_0=mean(X0);     % 原始序列平均值
Temp_0=(X0-AV_0).^2 ;
Temp_1=sum(Temp_0)/length(CA);
S1=sqrt(Temp_1)   ;     %原始序列的标准差
TempC=S2/S1*100;      %方差比
C=strcat(num2str(TempC),'%')   %后验差检验  %方差比    
%----------
SS=0.675*S1 ;
Delta=abs(CA-AV) ;
TempN=find(Delta<=SS);
N1=length(TempN);
N2=length(CA);
TempP=N1/N2*100;
P=strcat(num2str(TempP),'%')   %后验差检验    %计算小误差概率   

x = n-1:n;
y = b*exp(-a*x)+c;
y = diff(y);
next = y(length(y));

end
