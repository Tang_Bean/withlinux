function output = rbf (gamma, u, v)
% RBF kernel function

output = exp (-gamma * (u - v).^ 2);