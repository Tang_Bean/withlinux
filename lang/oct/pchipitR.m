function output = pchipitR (input)
% sizeof input 21 21 11

x = 1:21;
y = 1:21;
z = 1:11;

%R

xi = 1:.1:21;
yi = 1:.1:21;
zi = 1:.1:11;

output = zeros(201,201,101);

% round 1
for i = 1:21
    for j = 1:21
        fetch = reshape(input(i,j,:), 1,11);
        %interp1 (z, fetch, zi, 'pchip')
        output (  (i-1)*10+1 , (j-1)*10+1, : ) = interp1(z,fetch, zi, 'pchip');
    end
end

% round 2
for k = 1:101
    for i = 1:10:201
        %fetch = input( (floor((i-1)/10))+1,:,floor((k-1)/10)+1);
        fetch = [];
        for n = 1:10:201
            % squash
            fetch = [ fetch, output(i, n, k) ];
        end
        output ( i, :, k) = interp1(y, fetch, yi, 'pchip');
    end
end

% round 3
for j = 1:201
    for k = 1:101
        fetch = [];
        for n = 1:10:201
            % squash
            fetch = [ fetch, output(n,j,k) ];
        end
        output (:, j, k) = interp1 (x, fetch, xi, 'pchip');
    end
end
