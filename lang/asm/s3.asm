
N=100
stack segment stack 'stack'
      db 100h dup(?)
top   label word
stack ends

data  segment
table dw 3 dup(?)
banner  db 'init: input ready', 0ah, 0dh, '$'
msg1    db 'the max number is ', 0ah, 0dh, '$'
buffer  db 100 dup (?)
buffer2 db 0ah, 0dh
counter dw 0h
max     db 0h
;numbers dw 100 dup(?)
numbers dw N
	db N dup(?)
data ends

code segment
  assume cs:code, ds:data, es:data, ss:stack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; macro

regsave macro

push ax
push bx
push cx
push dx

push si
push di
push bp
endm

regrestore macro

pop bp
pop di
pop si

pop dx
pop cx
pop bx
pop ax
endm


start:

mov ax, data
mov ds, ax
mov es, ax
mov ax, stack
mov ss, ax
lea sp, top

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; user part


main:

; show banner

call far ptr printbanner

; read string
lea di, buffer
mov cx, 97d
mov counter, 0h
theloop0:
mov ah, 01h
int 21h
mov [di], al
inc di
inc counter
cmp al, 0dh
je escapeloop0
loop theloop0
escapeloop0:
;xor cx, cx
;mov [di], byte ptr 0ah
;mov [di], byte ptr 0dh
;mov [di], byte ptr '$'

; print string

; newline
mov ah, 02h
mov dl, 0ah
int 21h

lea si, buffer
mov cx, counter
theloop1:
mov ah, 02h
mov dl, [si]
inc si
int 21h
loop theloop1

call far ptr printbanner

;; generate random numbers @numbers
mov cx, numbers
lea si, numbers+2
mov bl, 1h
mov al, 64h
loop00:
mov [si], al
inc si
;add al, bl
dec al
loop loop00

; print numbers
mov dl, 0ah
mov ah, 02h
int 21h
int 21h

mov cx, numbers
lea si, numbers+2
loop41:
mov dl, [si]
;
shr dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
;
add dl, 30h
cmp dl, 3ah
jb cont411
add dl, 7h
cont411:
xor al, al
mov ah, 02h
int 21h ; print the high byte
mov dl, [si]
shl dl, 1
shl dl, 1
shl dl, 1
shl dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
add dl, 30h
cmp dl, 3ah
jb cont410
add dl, 7h
cont410:
xor al, al
mov ah, 02h
int 21h ; print the low byte
mov dl, 20h
int 21h
inc si
loop loop41
; end print numbers

; sort
mov cx, numbers
dec cx
lea si, numbers+2
add si, cx

lp1:
push cx
push si
lp2:
mov al,[si]
cmp al,[si-1]
jae noxchg
xchg al,[si-1]
mov [si], al
noxchg:
dec si
loop lp2
pop si
pop cx
loop lp1
; end sort 

mov ah, 02h
mov dl, 0ah
int 21h
call far ptr printbanner

; print numbers
mov dl, 0ah
mov ah, 02h
int 21h
int 21h

mov cx, numbers
lea si, numbers+2
loop51:
mov dl, [si]
;
shr dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
;
add dl, 30h
cmp dl, 3ah
jb cont511
add dl, 7h
cont511:
xor al, al
mov ah, 02h
int 21h ; print the high byte
mov dl, [si]
shl dl, 1
shl dl, 1
shl dl, 1
shl dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
shr dl, 1
add dl, 30h
cmp dl, 3ah
jb cont510
add dl, 7h
cont510:
xor al, al
mov ah, 02h
int 21h ; print the low byte
mov dl, 20h
int 21h
inc si
loop loop51
; end print numbers


;;;;;;;;;;;;;; return to OS
returntoos:
mov ah, 4ch
mov al, 0
int 21h

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; sub functions

printbanner proc far
regsave
mov ah, 9h
lea dx, banner
int 21h
regrestore
ret
printbanner endp

;;;; end
code ends
end start
