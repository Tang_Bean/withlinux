stack segment stack 'stack'
      dw 100h dup(?)
top   label word
stack ends

data  segment
table dw 3 dup(?)
banner  db 'init: input ready', 0ah, 0dh, '$'
string1 db 'English Name Zhou Mo', 0ah, 0dh, '$'
string2 db '13020610022 Zhou Mo', 0ah, 0dh, '$'
msg1    db 'invalid input, retry', 0ah, 0dh, '$'
data ends

code segment
  assume cs:code, ds:data, es:data, ss:stack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; macro

regsave macro

push ax
push bx
push cx
push dx

push si
push di
push bp
endm

regrestore macro

pop bp
pop di
pop si

pop dx
pop cx
pop bx
pop ax
endm


start:

mov ax, data
mov ds, ax
mov es, ax
mov ax, stack
mov ss, ax
lea sp, top

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; user part


main:

; show banner
call far ptr printbanner

; read key, and convert from ascii to int
mov ah, 1h
int 21h
sub al, 30h

; branch
jb main ; for invalid input
jz main
cmp al,4h
jnae branches
call far ptr printinvalid
jae main

branches:

cmp al, 1h
je stage1
cmp al, 2h
je stage2
cmp al, 3h
je stage3
jmp main

stage1:

call far ptr print1
jmp returntoos

stage2:

call far ptr print2
jmp returntoos

stage3:
jmp returntoos

;;;;;;;;;;;;;; return to OS
returntoos:
mov ah, 4ch
mov al, 0
int 21h

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; sub functions

printinvalid proc far
regsave
mov ah, 9h
lea dx, msg1
int 21h
regrestore
ret
printinvalid endp

print1 proc far
regsave
mov ah, 9h
lea dx, string1
int 21h
regrestore
ret
print1 endp

print2 proc far
regsave
mov ah, 9h
lea dx, string2
int 21h
regrestore
ret
print2 endp

printbanner proc far
regsave
mov ah, 9h
lea dx, banner
int 21h
regrestore
ret
printbanner endp

;;;; end
code ends
end start