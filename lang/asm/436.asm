; 4.36 ASM file
; 2015 Zhou Mo, 14.Dec

stack segment stack 'stack'
      dw 100h dup(?)
top   label word
stack ends

data  segment
xtable dw 10 dup(?)
table dw infom0, infom1, infom2, infom3, infom4, infom5, infom6, infom7, infom8, infom9
infom0 db 'Zhou Mo 9', 0ah, 0dh, '$'
infom1 db 'Zhou Mo 0', 0ah, 0dh, '$'
infom2 db 'zhou mo 1', 0ah, 0dh, '$'
infom3 db 'Zhou Mo 2', 0ah, 0dh, '$'
infom4 db 'Zhou Mo 3', 0ah, 0dh, '$'
infom5 db 'Zhou Mo 4', 0ah, 0dh, '$'
infom6 db 'Zhou Mo 5', 0ah, 0dh, '$'
infom7 db 'Zhou Mo 6', 0ah, 0dh, '$'
infom8 db 'Zhou Mo 7', 0ah, 0dh, '$'
infom9 db 'Zhou Mo 8', 0ah, 0dh, '$'
data ends

code segment
  assume cs:code, ds:data, es:data, ss:stack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; macro

regsave macro
push ax
push bx
push cx
push dx
push si
push di
push bp
endm

regrestore macro
pop bp
pop di
pop si
pop dx
pop cx
pop bx
pop ax
endm

_start:

mov ax, data
mov ds, ax
mov es, ax
mov ax, stack
mov ss, ax
lea sp, top

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; user part


main:

; show banner
;call far ptr printinfo0


mov cx, 10 ; 
lea di, table
_loop:
mov ah, 9h
mov dx, [di]
int 21h
inc di
inc di
loop _loop

jmp main

;;;;;;;;;;;;;; return to OS
returntoos:
mov ah, 4ch
mov al, 0
int 21h

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; sub functions

printinfo0 proc far
regsave
mov ah, 9h
lea dx, infom0
int 21h
regrestore
ret
printinfo0 endp

;;;; end
code ends
end _start
