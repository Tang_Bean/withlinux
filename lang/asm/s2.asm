stack segment stack 'stack'
      db 100h dup(?)
top   label word
stack ends

data  segment
table dw 3 dup(?)
banner  db 'init: input ready', 0ah, 0dh, '$'
msg1    db 'the max number is ', 0ah, 0dh, '$'
buffer  db 100 dup (?)
buffer2 db 0ah, 0dh
counter dw 0h
max     db 0h
data ends

code segment
  assume cs:code, ds:data, es:data, ss:stack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; macro

regsave macro

push ax
push bx
push cx
push dx

push si
push di
push bp
endm

regrestore macro

pop bp
pop di
pop si

pop dx
pop cx
pop bx
pop ax
endm


start:

mov ax, data
mov ds, ax
mov es, ax
mov ax, stack
mov ss, ax
lea sp, top

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; user part


main:

; show banner

call far ptr printbanner

; read string

lea di, buffer
mov cx, 97d
mov counter, 0h
theloop0:
mov ah, 01h
int 21h
mov [di], al
inc di
inc counter
cmp al, 0dh
je escapeloop0
loop theloop0
escapeloop0:
;xor cx, cx
;mov [di], byte ptr 0ah
;mov [di], byte ptr 0dh
;mov [di], byte ptr '$'

; print string

; newline
mov ah, 02h
mov dl, 0ah
int 21h

; scan string
lea si, buffer
mov cx, counter
theloop1:
;mov ah, 02h
mov dl, [si]
inc si
;int 21h
cmp dl, max
jb cont1
mov max, dl

cont1:
loop theloop1

; print the max
mov ah, 9h
lea dx, msg1
int 21h

mov ah, 2h
mov dl, max
int 21h

;;;;;;;;;;;;;; return to OS
returntoos:
mov ah, 4ch
mov al, 0
int 21h

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; sub functions


printbanner proc far
regsave
mov ah, 9h
lea dx, banner
int 21h
regrestore
ret
printbanner endp

;;;; end
code ends
end start
