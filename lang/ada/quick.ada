-- quick tour of Ada
-- http://www.adaic.org/learn/materials/intro/

-- Ada hello world

with Ada.Text_Io;
use Ada.Text_Io;

procedure Hello is
begin
	Put_Line("Hello ada!");
end Hello;
