## With Linux - Quite Messy Collection of Personal Snippets

> KISS: Keep It Simple, Stupid

This personal snippets collection contains not only hints, tricks and
hacks about Linux. However many files may be not useful to you.

To search among these notes, lookup keywords in this repo with the
perl utility `ack`. Or search some keywords with the script `search`
as long as you have created an xapian database with `./maintain xdb`.

The following listing is automatically generated.
# Top

* [Search Keyword In Xapian Databse](search)  

# Install

* [(/boot + Luks (lvm (/ + /home))) Linux Installation](linux/lvm-over-luks.md)  
* [Alpine Linux Note](linux/alpine.md)  
* [Archlinux's Application List](https://wiki.archlinux.org/index.php/List_of_applications)  
* [Bootstrap Minimal/stage3 Debian System](linux/bootstrap)  
* [Debian Sid Root On Zfs](linux/sidonzfs.md)  
* [Gentoo Prefix](https://wiki.gentoo.org/wiki/Project:Prefix)  
* [Gentoo: Sakaki's Efi Install Guide](https://wiki.gentoo.org/wiki/Sakaki%27s_EFI_Install_Guide)  
* [Graphic Card](linux/graphic_card_driver.txt)  
* [Install Linux Into An Usb Drive](linux/install-linux-into-usb-stick.txt)  
* [Mdadm Note For Creating Software Md5](linux/mdadm.md)  
* [Openbsd Note](linux/openbsd.md)  
* [Simple Archlinux Install Note](linux/arch.md)  
* [Some Packages Related To Efi/uefi](linux/efi.md)  
* [Tips About Solid State Drive (ssd)](linux/ssd.md)  
* [Turning Off Graphic Card](linux/turn-off-gpu.txt)  
* [Wireless Network Card](linux/wireless.txt)  

# Admin

* [802.1x Authentication](linux/802auth)  
* [Access Intranet With Ddns](linux/intranet-ddns.md)  
* [Apache2 (2.2.22) ... Simple Setup](linux/apache2.md)  
* [Apt/dpkg](linux/apt-dpkg.md)  
* [Archlinux Build System (abs)](linux/abs.md)  
* [Augment Existing Ext4 Filesystem](linux/ext4-extend-partition.md)  
* [Backlight](linux/backlight.txt)  
* [Bind9 (debian)... Dns (cache) - Config File](linux/named.conf.options)  
* [Blas](linux/blas.md)  
* [Comparison Of Dns Server Software](http://http://en.wikipedia.org/wiki/Comparison_of_DNS_server_software)  
* [Compiling Linux Kernel](linux/compile.txt)  
* [Config Runlevels (sysvinit)](linux/runlevel.txt)  
* [Cpu Freqency Tweak ... Hacky](linux/cpufreq.sh)  
* [Debian Wiki: High Performance Computing](https://wiki.debian.org/HighPerformanceComputing)  
* [Debianwiki:schroot](https://wiki.debian.org/Schroot)  
* [Dnsmasq ... Local Cache Name Server](linux/dnsmasq.md)  
* [Docker.io, Some Notes About The Linux Container](linux/docker.md)  
* [Emulating Systems With Qemu](linux/qemu-emulate.md)  
* [Font ... Install Font](linux/font.txt)  
* [Font In Tty](linux/tty-font.txt)  
* [Gitlab ... Official Deb Setup](linux/gitlab.md)  
* [Google Earth ... Install On Amd64](linux/gearth.txt)  
* [Grub ... Boot From Kali Iso Image](linux/grub_kali_iso.txt)  
* [Grub2 ... Location Of Config Files](linux/grub_config_file_location.txt)  
* [Hard Disk Link Power Manage ... Hacky](linux/hddpower.sh)  
* [Htcondor Note, Htcondor Is The Most Convenient One In Case To Manage A Single Machine](linux/condor.md)  
* [Iptables Howto ... Ubuntu Help](https://help.ubuntu.com/community/IptablesHowTo)  
* [Isatap ... Ipv6 With Isatap](linux/isatap.txt)  
* [Kali Tools Short List](linux/kali-tools.txt)  
* [Keyboard Setup](linux/kbd.md)  
* [Lapack](linux/lapack.md)  
* [Linux Frozen ... Handle With Sysrq](linux/sysrq.txt)  
* [List Of Dns Record Types ... Useful With Dig(1)](http://en.wikipedia.org/wiki/List_of_DNS_record_types)  
* [List Of Http Status Codes ... Wikipedia](http://en.wikipedia.org/wiki/List_of_HTTP_status_codes)  
* [Lvm Simple Note](linux/lvm.md)  
* [Making The Machine Silent ... No Beep](linux/silent_beep.txt)  
* [Microsoft: Teredo](https://technet.microsoft.com/en-us/library/bb457042.aspx)  
* [Miredo Note ... Ipv6 Net With Teredo](linux/miredo.md)  
* [Multiple Ssh Connection At The Same Time](linux/multiple-ssh.txt)  
* [Networkmanager ... Can't Change Network Settings?](linux/networkmanager.txt)  
* [Nmap Note](linux/nmap.txt)  
* [Note On Openmp And Openblas](linux/omp_oblas.md)  
* [Nping Note](linux/nping.txt)  
* [Openvpn ... Simple Utilization](linux/openvpn.txt)  
* [Pbs - Cluster Job Management](linux/pbs)  
* [Ping ... No Reply To Icmp Ping](linux/ping.txt)  
* [Port ... What Is Running On A Port](linux/port.txt)  
* [Prevent `resolv.conf` From Being Changed](linux/static_resolv_conf.md)  
* [Qemu/kvm -- Nographic Mode](linux/qemu-nographic.md)  
* [Restrict Connections With Iptables](linux/iptables-restrict-connections.md)  
* [Sata Hard Disk Hotplugging](linux/hotplug.md)  
* [Schroot Note, Unlike Plain Chroot](linux/schroot.md)  
* [Schroot: Archwiki:schroot](https://wiki.archlinux.org/index.php/Install_bundled_32-bit_system_in_64-bit_system)  
* [Share Ipv4 Over Ipv6 Tunnel](linux/share-ipv4-with-ipv6-tunnel.txt)  
* [Share The Network With Other Machine ... One Of Cases](linux/share-network-between-linux.txt)  
* [Smart, Hard Disk Smart Attributes](linux/smart.md)  
* [Some Open Source Fonts](linux/font_list.txt)  
* [Some Raid0 Restoring Experiment](linux/raid0rescue.md)  
* [Sshd Security ... Sshguard](linux/sshguard.txt)  
* [Sshd Sftp Chroot](linux/ssh.md)  
* [Steam ... Install Steam On Amd64](linux/steam.txt)  
* [Storm Local Setup ... Apache Inqubator, Storm](linux/setup-local-storm.txt)  
* [Sudo ...](linux/sudo.txt)  
* [Systemd ... Some Reference Link](linux/systemd_link.txt)  
* [Systemd Note](linux/systemd.md)  
* [Time ... Hardware Time](linux/hardwaretime.txt)  
* [Tzdata ... Change The System Time Zone](linux/time_zone_change.txt)  
* [Ufw -- Uncomplicated Firewall](linux/ufw.md)  
* [Virtualbox Shared Directory](linux/vbox-shared-dir.md)  
* [Virtualbox: Extremely Slow Network Transmission Speed Within Loop Between Host And Guest](linux/virt-slow-transmission.md)  
* [Vm Note](linux/vm.md)  
* [Vpn ... Set Up Vpn On Debian](linux/vpn.txt)  
* [Wine ... On Amd64](linux/wine.md)  
* [Zfs Note](linux/zfs.md)  

# Util

* [A Git Tutor ... Git Immersion](http://gitimmersion.com/)  
* [A Long List Of Linux Utilities](linux/util_list.md)  
* [Arch: Wine](https://wiki.archlinux.org/index.php/Wine)  
* [Bash Tricks](linux/bash_tricks.txt)  
* [Busybox, All-in-one Software, Developer Works](http://www.ibm.com/developerworks/cn/linux/l-busybox/index.html)  
* [Command Line Todo Manager, See Task-tutorial(5)](linux/task.md)  
* [Commands For Fun](linux/funny_commands.txt)  
* [Cpio Note](linux/cpio.md)  
* [Data Recovery Tools](linux/data-recover.txt)  
* [Encrypt Directory With Ecryptfs](linux/ecryptfs.md)  
* [Encrypting Disk With Luks](linux/disk-crypt.txt)  
* [Example Usage Of Avconv, An Alternative To Ffmpeg](linux/avconv.txt)  
* [Example Usage Of Netcat](linux/netcat.txt)  
* [Extend Your Screen Via Network](linux/extend-screen-with-vnc.md)  
* [Ffmpeg, Resize Picture And Video](linux/ffmpeg_resize_picture.txt)  
* [Flash Solution For Debian](linux/flash.md)  
* [Git ... Merge 2 Repos Into 1](linux/git_merge_repo.txt)  
* [Gnome Scaling Factor](linux/gnome-scale.md)  
* [Gnome Top Bar Height](linux/topbar.md)  
* [Gnu Utils - Powerful](linux/gnuutils.md)  
* [Gpg Best Practices (el)](https://help.riseup.net/en/security/message-security/openpgp/best-practices#self-signatures-should-not-use-sha1)  
* [Gpg Short Note](linux/short_gpg.md)  
* [Iceweasel And Chromium ... Cache Config, Of Iceweasel (firefox)](linux/iceweasel-cache.txt)  
* [Imagemagick, Resize, Trans-format, Rotate Picture](linux/imagemagick.txt)  
* [Input Greek Letters](linux/greek.md)  
* [Jpeg Integrity Check](linux/jpeg-int.md)  
* [Linux-perf](linux/perf.md)  
* [Lxde Shortcut Keys](linux/lxde-shortcut)  
* [Remove Data Safely](linux/remove-data-safely.txt)  
* [Set Up Git ... Github Help](https://help.github.com/articles/set-up-git)  
* [Sharing Files Over Network](linux/copyutil.md)  
* [Ssh-agent, Let It Memorize Your Ssh Password](linux/ssh-agent.txt)  
* [Terminology -- Fancy Terminal Emulator](linux/terminology.md)  
* [Terraria With Wine32, On Linux](linux/terraria.md)  
* [Tmux / Gnu Screen Note](linux/tmux.md)  
* [Using Vim, Convert Code Into Html](linux/vim_html.txt)  
* [Vim](linux/vim.md)  
* [Xorg+dwm Notes](linux/xorgdwm.md)  
* [Xset, Change Keyboard Input Delay/rate Under X](linux/keyrate)  

# Debian

* [Computer Language Benchmark Game](http://benchmarksgame.alioth.debian.org/)  
* [Debian Cd/dvd Hint](debian/debiancd.md)  
* [Debian Lua Package Policy](http://pkg-lua.alioth.debian.org/policy.html)  
* [Debian Python Policy](https://www.debian.org/doc/packaging-manuals/python-policy/)  
* [Debian Science Policy](http://debian-science.alioth.debian.org/debian-science-policy.html)  
* [Ftpmaster Removal](https://wiki.debian.org/ftpmaster_Removals)  
* [Gnu's License List](https://www.gnu.org/licenses/license-list.html)  
* [Gpg: Keysigning](debian/gpg.md)  
* [Gpl-faq](http://www.gnu.org/licenses/gpl-faq.html#NoticeInSourceFile)  
* [Hardening (wiki)](https://wiki.debian.org/Hardening)  
* [How To Get Backtrace (wiki)](https://wiki.debian.org/HowToGetABacktrace)  
* [Library Packaging Guide](https://www.netfort.gr.jp/~dancer/column/libpkg-guide/libpkg-guide.html)  
* [Mentors Faq](https://wiki.debian.org/DebianMentorsFaq)  
* [Package Transition](https://wiki.debian.org/PackageTransition)  
* [Python Library Style Guide](https://wiki.debian.org/Python/LibraryStyleGuide)  
* [Rpath Issue](https://wiki.debian.org/RpathIssue)  
* [Setup Debian Mirror](https://www.debian.org/mirror/ftpmirror)  
* [Setup Unofficial Debian Archive](debian/unofficial_archive.txt)  
* [Upstream Guide](https://wiki.debian.org/UpstreamGuide)  
* [Wikipedia: Comparison Of Licenses](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses)  

# Prog

* [Cmake](http://www.cmake.org/cmake-tutorial/)  
* [Gnu Make](http://www.gnu.org/software/make/manual/make.html)  
* [Osamu Aoki's Fun To Program](https://people.debian.org/~osamu/fun2prog.html)  
* [Rainbowlog In Bash](lang/lumin_log.sh)  
* [Rainbowlog In Lua](lang/lua/logging/lumin_log.lua)  
* [Rainbowlog In Python3](lang/py3/lumin_log_demo.py)  

# SubProject

* [C Util To Chdir Into An Archive](cda)  
* [C Util To Show Byte Frequency Of A Given File](bytefreq)  

# Resource

* [Arch Wiki](https://wiki.archlinux.org)  
* [Awesome List](https://github.com/sindresorhus/awesome)  
* [Debian Admin](https://debian-administration.org/)  
* [Debian Wiki](https://wiki.debian.org)  
* [Freebsd Doc ... Sometimes Helps](https://www.freebsd.org/docs.html)  
* [Funtoo Wiki](https://wiki.funtoo.org)  
* [Gentoo Doc](http://www.gentoo.org/doc)  
* [Gentoo Wiki](https://wiki.gentoo.org/wiki/Main_Page)  
* [Ibm Developer Works](https://www.ibm.com/developerworks/cn/linux/)  
* [Linux Kernel Document](https://www.kernel.org/doc)  
* [Stackoverflow](http://stackoverflow.com)  
* [Vbird.org ... Detailed, Complete Linux Guide](http://linux.vbird.org)  
* [Wikipedia](http://wikipedia.org)  

## LICENSE

```
The WithLinux Project is published under the MIT LICENSE.
COPYRIGHT (C) 2014-2017 Lumin

Created on 2014 June 28
Have fun with UNIX, learn from UNIX, but never become UNIX.
```  
