# Genetic algorithm using Julia 0.6.2

# hyper parameters for the algorithm
N = 10 # 2k
MAXITER = 1000
MUTATION = 0.1

# object function
f(x1,x2) = 21.5 + x1*sin(4pi*x1) + x2*sin(20pi*x2)

# generate initial population
function getInitIndividual()
	[rand([0,1]) for _ in 1:33]
end
function strIndividual(ind)
	join(map(x->string(x), ind))
end
function dumpPop(pop)
	for i in 1:length(pop)
		println(i, '\t', strIndividual(pop[i]))
	end
end
population = []
for i in 1:N
	ind = getInitIndividual()
	println(i, '\t', strIndividual(ind))
	push!(population, ind)
end
println()

# evaluation function
function evalIndividual(ind, verbose=false)
	x1spl = ind[1:18] # 18 bits
	x2spl = ind[19:33] # 15 bits
	x1nrm = sum([x << (i-1) for (i,x) in enumerate(x1spl)])
	x2nrm = sum([x << (i-1) for (i,x) in enumerate(x2spl)])
	x1 = -3.0 + x1nrm * (12.1-(-3.0)) / (2^18 - 1)
	x2 = 4.1 + x2nrm * (5.8 - 4.1) / (2^15 - 1)
	if verbose
		println(strIndividual(ind), '\t', x1, '\t', x2, '\t', f(x1, x2)) # dbg
	end
	f(x1, x2)
end
function getScores(pop)
	map(x->evalIndividual(x), population)
end

# selection function
function cummulativeProb(sc)
	[sum(sc[1:i])/sum(sc) for i in 1:length(sc)]
end
function getNextGen(pop, cummprob)
	nextgen = []
	for i in 1:length(pop)
		p = rand()
		for (j,x) in enumerate(cummprob)
			if p < cummprob[j]
#				println("$(p), sel idx $(j)") # dbg
				push!(nextgen, pop[j])
				break
			end
		end
	end
	nextgen
end

# crossover function
function crossoverOneCut!(pop)
	shuffle!(pop)
	for i in 1:2:length(pop) # crossover (i, i+1)
		cutpoint = rand(1:33)
		newa = pop[i][1:cutpoint]
		newb = pop[i+1][1:cutpoint]
		push!(newa, pop[i+1][cutpoint+1:end]...)
		push!(newb, pop[i][cutpoint+1:end]...)
		pop[i], pop[i+1] = newa, newb
	end
end

# mutation function
flip(x) = x == 0 ? 1 : 0
function mutation!(pop)
	for i in 1:length(pop)
		if rand() > MUTATION
			continue
		end
		for j in 1:rand(1:33)
			mindex = rand(1:33)
			pop[i][mindex] = flip(pop[i][mindex])
		end
	end
end

# main loop
fhistmax = []
fcurmax = []
bestind = []
for iter in 1:MAXITER
	# evaluation
	scores = getScores(population)
	if 0 == length(fhistmax)
		bestind = population[indmax(scores)]
	else
		if maximum(scores) > fhistmax[end]
			bestind = population[indmax(scores)]
		end
	end
	# selection
	cp = cummulativeProb(scores)
	population = getNextGen(population, cp)
	#println("after selection")
	#dumpPop(population)
	# crossover
	crossoverOneCut!(population)
	# mutation
	mutation!(population)
	# save data for plots
	push!(fcurmax, maximum(scores))
	if 0 == length(fhistmax)
		push!(fhistmax, fcurmax[end])
	else
		push!(fhistmax, maximum([fhistmax[end], fcurmax[end]]))
	end
	# report
#	dumpPop(population)
	println("iteration $(iter)\t max score $(maximum(scores))\t historical max $(fhistmax[end])")
	println()
end

# dump data
println(fcurmax)
println(fhistmax)
writedlm("cur.txt", fcurmax)
writedlm("hist.txt", fhistmax)

# dump solution
evalIndividual(bestind, true)
