t1 = []
t2 = []
t3 = []
t4 = []
sz = [2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10,2^11,2^12]
for n = sz 
	A = rand(n);
	B = rand(n);
	tic;
	C = A * B;
	t1 = [t1, toc()]
end
t1

for n = sz(1:8)
	A = rand(n);
	B = rand(n);
	tic;
	BT = B.';
	C = zeros(n);
	for i=1:n
		for j=1:n
			for k=1:n
				C(i,j) = C(i,j) + A(i,k) * BT(j,k);
			end
		end
	end
	t2 = [t2, toc()]
end
t2

for n = sz
	A = rand(n);
	B = rand(n);
	tic;
	A11 = A(1:n/2,1:n/2);
	A12 = A(1:n/2,n/2+1:n);
	A21 = A(n/2+1:n,1:n/2);
	A22 = A(n/2+1:n,n/2+1:n);
	B11 = B(1:n/2,1:n/2);
	B12 = B(1:n/2,n/2+1:n);
	B21 = B(n/2+1:n,1:n/2);
	B22 = B(n/2+1:n,n/2+1:n);
	C3 = zeros(n);
	C11 = A11*B11+A12*B21;
	C12 = A11*B12+A12*B22;
	C21 = A21*B11+A22*B21;
	C22 = A21*B12+A22*B22;
	C3 = [C11 C12;C21 C22];
	t3 = [t3, toc()]
end
t3

for n = sz
	A = rand(n);
	B = rand(n);
	tic;
	C4 = zeros(n);
	A11 = A(1:n/2,1:n/2);
	A12 = A(1:n/2,n/2+1:n);
	A21 = A(n/2+1:n,1:n/2);
	A22 = A(n/2+1:n,n/2+1:n);
	B11 = B(1:n/2,1:n/2);
	B12 = B(1:n/2,n/2+1:n);
	B21 = B(n/2+1:n,1:n/2);
	B22 = B(n/2+1:n,n/2+1:n);
	M1 = A11*(B12-B22);
	M2 = (A11+A12)*B22;
	M3 = (A21+A22)*B11;
	M4 = A22*(B21-B11);
	M5 = (A11+A22)*(B11+B22);
	M6 = (A12-A22)*(B21+B22);
	M7 = (A11-A21)*(B11+B12);
	C11 = M5+M4-M2+M6;
	C12 = M1+M2;
	C21 = M3+M4;
	C22 = M5+M1-M3-M7;
	C4 = [C11 C12;C21 C22];
	t4 = [t4, toc()]
end
t4

figure
hold on
plot(log(sz)/log(2), t1, '.-k');
plot(log(sz(1:8))/log(2), t2, '.-b');
plot(log(sz)/log(2), t3, '.-r');
plot(log(sz)/log(2), t4, '.-g');
xlabel('matrix size 2^x');
ylabel('time');
legend('method1', 'method2', 'method3', 'method4')
title('Elapsed Time of Matrix Multiplication');
print -dpng h2.png
