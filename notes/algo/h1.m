iter = 100
for n = [10,50,100,150,200,300,400,500,10000,20000,50000,100000]
	% construct the polynomial
	A = randn(n+1, 1);
	T = [];
	% run several times and find the mean time
	for i = 1:iter
		X = ones(n+1, 1);
		x = rand();
		tic();
		for j = n:-1:1
			X(j) = x * X(j+1);
		end
		P_x = dot(A, X);
		T = [T; toc()];
	end
	Tmean = mean(T);
	disp(sprintf('n = %d, Tmean = %f', n, Tmean))
end
