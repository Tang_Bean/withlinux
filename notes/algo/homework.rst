.. 算法大作业
.. ==========

.. role:: latex(raw)
   :format: latex

:latex:`\begin{center}\Huge{算法设计技术与方法}\end{center}\vskip 1cm`

:latex:`\begin{center}\Large{2018年1月5日}\end{center}\vskip 1cm`

平台说明：本作业采用的CPU为I7-7440HQ，操作系统为64位Debian Linux unstable，
主要代码实现采用Julia语言，对应的Julia版本为0.6.2，对比用的Matlab版本为
R2017a linux/x64。为了简便，本作业中的实现均为串行实现，没有并行化。

作业一：实现多项式取值的四种运算，针对不同规模的输入值a，计算各种算法的
运行时间，问题规模n分别取10,50,100,150,200,300,400,500,10000,20000,50000,
100000,绘制四种算法运行时间的比较图。

  (1) 直接计算::

         # using Julia 0.6.2
         iter = 100
         println("$(iter) iterations for each n")
         for n in (10,50,100,150,200,300,400,500,10000,20000,50000,100000)
         	# construct the polynomial
         	A = randn!(Array{Float64}(n+1)) # coefficients of the polynomial
         	T = Array{Float64}(0)
         	# run several times and find the mean time
         	for i in 1:iter
         		X = fill!(Array{Float64}(n+1), 1)
         		x = Float64(rand()) # parameter of the polynomial
         		tic()
         		for j in 2:n+1
         			X[j] = x * X[j-1]
         		end
         		P_x = dot(A, X)
         		push!(T, toq())
         	end
         	Tmean = mean(T)
         	println("n = $(n), Tmean = $(Tmean)")
         end

  (2) :math:`P_n(x) = P_{n-1}(x) + a_nx^n`::

         #using Julia 0.6.2
         iter = 100
         println("$(iter) iterations for each n")
         for n in (10,50,100,150,200,300,400,500,10000,20000,50000,100000)
         	# construct the polynomial
         	A = randn!(Array{Float64}(n+1))
         	T = Array{Float64}(0)
         	# run several times and find the mean time
         	for i in 1:iter
         		x = Float64(rand())
         		P_x = 0.
         		tic()
         		for j in 1:n+1
         			P_x += A[j] * x^(j-1)
         		end
         		push!(T, toq())
         	end
         	Tmean = mean(T)
         	println("n = $(n), Tmean = $(Tmean)")
         end

  (3) :math:`P_n(x) = P_{n-1}(x) + a_nx^n` ，并避免:math:`x_i` 的重复计算::

         #using Julia 0.6.2
         iter = 100
         println("$(iter) iterations for each n")
         for n in (10,50,100,150,200,300,400,500,10000,20000,50000,100000)
         	# construct the polynomial
         	A = randn!(Array{Float64}(n+1))
         	T = Array{Float64}(0)
         	# run several times and find the mean time
         	for i in 1:iter
         		x = Float64(rand())
         		P_x = 0.
         		xj = 1.
         		tic()
         		for j in 1:n+1
         			P_x += xj * A[j]
         			xj *= x
         		end
         		push!(T, toq())
         	end
         	Tmean = mean(T)
         	println("n = $(n), Tmean = $(Tmean)")
         end

  (4) :math:`P_n(x) = x P_{n-1}(x) + a_n`::

         #using Julia 0.6.2
         iter = 100
         println("$(iter) iterations for each n")
         for n in (10,50,100,150,200,300,400,500,10000,20000,50000,100000)
         	# construct the polynomial
         	A = randn!(Array{Float64}(n+1))
         	T = Array{Float64}(0)
         	# run several times and find the mean time
         	for i in 1:iter
         		x = Float64(rand())
         		P_x = A[end]
         		tic()
         		for j in n:-1:1
         			P_x = x * P_x + A[j]
         		end
         		push!(T, toq())
         	end
         	Tmean = mean(T)
         	println("n = $(n), Tmean = $(Tmean)")
         end

  (5) 实验结果图表

      .. image:: h1_data.png

      .. image:: h1.png

      综合代码实现以及实验结果，方法4的计算速度最快，并且远超方法1
      和方法2。方法3的速度非常接近方法4。

:latex:`\newpage`

作业二：分别实现矩阵相乘的三种算法，比较三种算法在矩阵大小为2的2,3,4,5,6,7,8,9,
10,11,12次方时的运行时间与MATLAB自带矩阵相乘的运行时间，绘制对比图。

  三种算法分别为直接计算和两种分治算法，以下为算法实现：
  ::

    t1 = []
    t2 = []
    t3 = []
    t4 = []
    sz = [2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10,2^11,2^12]
    for n = sz 
    	A = rand(n);
    	B = rand(n);
    	tic;
    	C = A * B;
    	t1 = [t1, toc()]
    end
    t1
    
    for n = sz(1:8)
    	A = rand(n);
    	B = rand(n);
    	tic;
    	BT = B.';
    	C = zeros(n);
    	for i=1:n
    		for j=1:n
    			for k=1:n
    				C(i,j) = C(i,j) + A(i,k) * BT(j,k);
    			end
    		end
    	end
    	t2 = [t2, toc()]
    end
    t2
    
    for n = sz
    	A = rand(n);
    	B = rand(n);
    	tic;
    	A11 = A(1:n/2,1:n/2);
    	A12 = A(1:n/2,n/2+1:n);
    	A21 = A(n/2+1:n,1:n/2);
    	A22 = A(n/2+1:n,n/2+1:n);
    	B11 = B(1:n/2,1:n/2);
    	B12 = B(1:n/2,n/2+1:n);
    	B21 = B(n/2+1:n,1:n/2);
    	B22 = B(n/2+1:n,n/2+1:n);
    	C3 = zeros(n);
    	C11 = A11*B11+A12*B21;
    	C12 = A11*B12+A12*B22;
    	C21 = A21*B11+A22*B21;
    	C22 = A21*B12+A22*B22;
    	C3 = [C11 C12;C21 C22];
    	t3 = [t3, toc()]
    end
    t3
    
    for n = sz
    	A = rand(n);
    	B = rand(n);
    	tic;
    	C4 = zeros(n);
    	A11 = A(1:n/2,1:n/2);
    	A12 = A(1:n/2,n/2+1:n);
    	A21 = A(n/2+1:n,1:n/2);
    	A22 = A(n/2+1:n,n/2+1:n);
    	B11 = B(1:n/2,1:n/2);
    	B12 = B(1:n/2,n/2+1:n);
    	B21 = B(n/2+1:n,1:n/2);
    	B22 = B(n/2+1:n,n/2+1:n);
    	M1 = A11*(B12-B22);
    	M2 = (A11+A12)*B22;
    	M3 = (A21+A22)*B11;
    	M4 = A22*(B21-B11);
    	M5 = (A11+A22)*(B11+B22);
    	M6 = (A12-A22)*(B21+B22);
    	M7 = (A11-A21)*(B11+B12);
    	C11 = M5+M4-M2+M6;
    	C12 = M1+M2;
    	C21 = M3+M4;
    	C22 = M5+M1-M3-M7;
    	C4 = [C11 C12;C21 C22];
    	t4 = [t4, toc()]
    end
    t4
    
    figure
    hold on
    plot(log(sz)/log(2), t1, '.-k');
    plot(log(sz(1:8))/log(2), t2, '.-b');
    plot(log(sz)/log(2), t3, '.-r');
    plot(log(sz)/log(2), t4, '.-g');
    xlabel('matrix size 2^x');
    ylabel('time');
    legend('method1', 'method2', 'method3', 'method4')
    title('Elapsed Time of Matrix Multiplication');
    print -dsvg h2.svg

  结果如下图所示

  .. image:: h2.png

  图中method1为Matlab的Native算法，method2为直接计算，method3和method4为两种
  分治算法。

:latex:`\newpage`

作业三：遗传算法 :math:`\max f(x_1, x_2) = 21.5 + x\cdot \sin(4\pi x_1) + x_2 \cdot sin(20\pi x_2)`
:math:`s.t. -3.0 \leq x \leq 12.1`, :math:`4.1 \leq x_2 \leq 5.8`


  (1) Julia代码实现
  ::

    # Genetic algorithm using Julia 0.6.2
    
    # hyper parameters for the algorithm
    N = 10 # 2k
    MAXITER = 1000
    MUTATION = 0.1
    
    # object function
    f(x1,x2) = 21.5 + x1*sin(4pi*x1) + x2*sin(20pi*x2)
    
    # generate initial population
    function getInitIndividual()
    	[rand([0,1]) for _ in 1:33]
    end
    function strIndividual(ind)
    	join(map(x->string(x), ind))
    end
    function dumpPop(pop)
    	for i in 1:length(pop)
    		println(i, '\t', strIndividual(pop[i]))
    	end
    end
    population = []
    for i in 1:N
    	ind = getInitIndividual()
    	println(i, '\t', strIndividual(ind))
    	push!(population, ind)
    end
    println()
    
    # evaluation function
    function evalIndividual(ind)
    	x1spl = ind[1:18] # 18 bits
    	x2spl = ind[19:33] # 15 bits
    	x1nrm = sum([x << (i-1) for (i,x) in enumerate(x1spl)])
    	x2nrm = sum([x << (i-1) for (i,x) in enumerate(x2spl)])
    	x1 = -3.0 + x1nrm * (12.1-(-3.0)) / (2^18 - 1)
    	x2 = 4.1 + x2nrm * (5.8 - 4.1) / (2^15 - 1)
    #	println(strIndividual(ind), '\t', x1, '\t', x2, '\t', f(x1, x2)) # dbg
    	f(x1, x2)
    end
    function getScores(pop)
    	map(x->evalIndividual(x), population)
    end
    
    # selection function
    function cummulativeProb(sc)
    	[sum(sc[1:i])/sum(sc) for i in 1:length(sc)]
    end
    function getNextGen(pop, cummprob)
    	nextgen = []
    	for i in 1:length(pop)
    		p = rand()
    		for (j,x) in enumerate(cummprob)
    			if p < cummprob[j]
    #				println("$(p), sel idx $(j)") # dbg
    				push!(nextgen, pop[j])
    				break
    			end
    		end
    	end
    	nextgen
    end
    
    # crossover function
    function crossoverOneCut!(pop)
    	shuffle!(pop)
    	for i in 1:2:length(pop) # crossover (i, i+1)
    		cutpoint = rand(1:33)
    		newa = pop[i][1:cutpoint]
    		newb = pop[i+1][1:cutpoint]
    		push!(newa, pop[i+1][cutpoint+1:end]...)
    		push!(newb, pop[i][cutpoint+1:end]...)
    		pop[i], pop[i+1] = newa, newb
    	end
    end
    
    # mutation function
    flip(x) = x == 0 ? 1 : 0
    function mutation!(pop)
    	for i in 1:length(pop)
    		if rand() > MUTATION
    			continue
    		end
    		for j in 1:rand(1:33)
    			mindex = rand(1:33)
    			pop[i][mindex] = flip(pop[i][mindex])
    		end
    	end
    end
    
    # main loop
    fhistmax = []
    fcurmax = []
    for iter in 1:MAXITER
    	# evaluation
    	scores = getScores(population)
    	# selection
    	cp = cummulativeProb(scores)
    	population = getNextGen(population, cp)
    	#println("after selection")
    	#dumpPop(population)
    	# crossover
    	crossoverOneCut!(population)
    	# mutation
    	mutation!(population)
    	# save data for plots
    	push!(fcurmax, maximum(scores))
    	if 0 == length(fhistmax)
    		push!(fhistmax, fcurmax[end])
    	else
    		push!(fhistmax, maximum([fhistmax[end], fcurmax[end]]))
    	end
    	# report
    #	dumpPop(population)
    	println("iteration $(iter)\t max score $(maximum(scores))\t historical max $(fhistmax[end])")
    	println()
    end
    
    # dump data
    println(fcurmax)
    println(fhistmax)
    writedlm("cur.txt", fcurmax)
    writedlm("hist.txt", fhistmax)

  (2)结果及图

    优化结果为 111000000000011111000111101101011, 即
    :math:`x_1=11.628584017120426`, :math:`x_2=5.527362895596179`,
    :math:`f_max=38.58335007847087`.

  .. image:: h3.png

