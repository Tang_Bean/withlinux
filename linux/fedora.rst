Fedora Tips
===========

Package Management

  rpm as back-end, dnf as front-end. The old front-end yum is seemingly
  deprecated.

Source

  Find a mirror site and look for its usage help. Change files under
  ``/etc/yum.repos.d/`` directory.
