Port forwarding
===

# Via SSH

```
-C compression
-N do not execute remote command
-f go background
-g allow remote hosts to connect to local forwarded ports.
```

```
# Local Forwarding
# Connections to local:6666 will be forwarded to remote:22
ssh -L 6666:remote:22 master@local -fgNC
```

```
# Remote Forwarding
# Connections to remote:6666 will be forwarded to local:22
ssh -R 6666:local:22 root@remote -fgNC
```

# troubleshooting

When `ssh -R` always bind the loopback address on the target machine
whatsoever command you typed, check the "GatewayPorts" option of the
sshd config on the target machine.
